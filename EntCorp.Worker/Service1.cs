﻿using EntCorp.Data;
using EntCorp.Repository;
using EntCorp.Service;
using EntCorp.Utils;
using System;
using System.ServiceProcess;
using System.Timers;

namespace EntCorp.Worker
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer(); 
        EntCorpContext _context; 
        IRepositoryWrapper _repoWrapper;
        DeviceService _deviceService;
        SyncDevicesService _syncService;

        public Service1()
        {
            InitializeComponent();
            _context = new EntCorpContext();
            _repoWrapper = new IRepositoryWrapper(_context); 
            _deviceService = new DeviceService(_repoWrapper);
            _syncService = new SyncDevicesService(_repoWrapper);
        }

        protected override void OnStart(string[] args)
        {
            DotNetEnv.Env.Load(@"c:\EntCorporate\worker\.env");

            int interval = DotNetEnv.Env.GetInt("SyncInterval");
            Logger.LogInfo("Start Worker!");
            Logger.LogInfo($"Sync interval is {interval} second");
            SyncData(this, null);
            timer.Elapsed += new ElapsedEventHandler(SyncData);
            timer.Interval = interval * 1000;
            timer.Enabled = true;
        }

        private async void SyncData(object sender,ElapsedEventArgs e)
        {
            try
            {
                string result = await _syncService.GetData();
            }
            catch(Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        protected override void OnStop()
        {
            Logger.LogWarn("Service has stopped!");
        }
    }
}
