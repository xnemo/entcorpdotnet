﻿using EntCorp.Forms;
using System;
using System.Windows.Forms;
using EntCorp.Helpers;
using EntCorp.Repository;

namespace EntCorp
{
    public partial class MainForm : Form
    {
        private IRepositoryWrapper _repoWrapper;
        FormsManager fm;
        public MainForm(IRepositoryWrapper repoWrapper)
        {
            InitializeComponent();
            fm = new FormsManager();
            _repoWrapper = repoWrapper;

        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            fm.CreateMDIChild(this, new DepartmentsForm(_repoWrapper));
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //fm.CreateMDIChild(this, new DepartmentsForm(_repoWrapper));
            fm.CreateMDIChild(this, new ReportsForm(_repoWrapper));
        }

        private void navBarItem2_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            fm.CreateMDIChild(this, new WorkScheduleForm(_repoWrapper));
        }

        private void пользователиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fm.CreateMDIChild(this, new UserForm());
        }

        private void navBarItem3_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            fm.CreateMDIChild(this, new AdminForm(_repoWrapper));
        }

        private void navBarItem4_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            fm.CreateMDIChild(this, new ReportsForm(_repoWrapper));
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void setInfo(string info)
        {
            infoBar.ForeColor = System.Drawing.Color.Black;
            infoBar.Text = info;
        }

        public void setAlert(string info)
        {
            infoBar.ForeColor = System.Drawing.Color.Red;
            infoBar.Text = info;
        }

        private void infoBar_TextChanged(object sender, EventArgs e)
        {
            //Task.Delay(TimeSpan.FromMilliseconds(10000))
            //    .ContinueWith(task => infoBar.Text = "");
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (e.CloseReason == CloseReason.UserClosing)
            //{
            //    this.Hide();
            //    e.Cancel = true;
            //}
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void логинToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AuthForm authForm = new AuthForm(_repoWrapper);
            authForm.ShowDialog();
        }
    }
}
