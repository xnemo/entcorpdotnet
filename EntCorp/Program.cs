﻿using EntCorp.Data;
using EntCorp.Repository;
using System;
using System.Windows.Forms;

namespace EntCorp
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {   
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            EntCorpContext _context = new EntCorpContext();
            IRepositoryWrapper _repoWrapper = new IRepositoryWrapper(_context);

            SingleInstance.SingleApplication.Run(new MainForm(_repoWrapper));
            //Application.Run(new MainForm());
        }
    }
}
