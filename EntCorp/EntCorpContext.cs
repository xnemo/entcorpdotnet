﻿using EntCorp.Entity;
using EntityFramework.Triggers;
using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntCorp.Context
{
    public class EntCorpContext : DbContextWithTriggers
    {
        public EntCorpContext(): base()
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Employe> Employes { get; set; }
        public DbSet<Holiday> Holidays { get; set; }
        public DbSet<WorkSchedule> WorkSchedules { get; set; }
        public DbSet<WorkSchema> WorkSchemas { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<Admission> Admissions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // configures one-to-many relationship
            modelBuilder.Entity<Employe>()
                .HasRequired<Department>(e => e.Department)
                .WithMany(d => d.Employes)
                .HasForeignKey<int>(e => e.DepartmentId)
                .WillCascadeOnDelete(false)
                ;

            modelBuilder.Entity<Employe>()
                .HasRequired<WorkSchedule>(e => e.WorkSchedule)
                .WithMany(w => w.Employes)
                .HasForeignKey<int>(e => e.WorkScheduleId)
                .WillCascadeOnDelete(false)
                ;

            modelBuilder.Entity<Department>()
                .HasRequired<WorkSchedule>(d => d.WorkSchedule)
                .WithMany(w => w.Departments)
                .HasForeignKey<int>(d => d.WorkScheduleId)
                .WillCascadeOnDelete(false)
                ;

            modelBuilder.Entity<WorkSchema>()
                .HasRequired<WorkSchedule>(ws => ws.WorkSchedule)
                .WithMany(w => w.WorkSchemas)
                .HasForeignKey<int>(ws => ws.WorkScheduleId)
                .WillCascadeOnDelete(false);
        }
    }
}
