﻿using System.Windows.Forms;

namespace EntCorp.Helpers
{
    class FormsManager
    {
        public void CreateMDIChild(Form parentForm, Form childForm, FormWindowState state = FormWindowState.Maximized)
        {
            bool formOpened = false;

            foreach (Form frm in parentForm.MdiChildren)
            {
                if (frm.GetType() == childForm.GetType())
                {
                    formOpened = true;
                    frm.WindowState = FormWindowState.Maximized;
                }
                else
                {
                    frm.WindowState = FormWindowState.Minimized;
                }
            }
            
            if (formOpened == false)
            {
                childForm.MdiParent = parentForm;
                //childForm.Owner = parentForm;
                childForm.Show();
                childForm.WindowState = state;
            }
        }
    }
}
