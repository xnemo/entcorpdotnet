﻿using System.Linq;
using System.Windows.Forms;

namespace EntCorp.Helpers
{
    public static class Alerts
    {
        public static void SetMessage(string message)
        {
            var frm = Application.OpenForms.Cast<Form>().Where(x => x.Name == "MainForm").FirstOrDefault();
            MainForm mf = (MainForm)frm;
            mf.setInfo(message);
        }

        public static void SetAlert(string message)
        {
            var frm = Application.OpenForms.Cast<Form>().Where(x => x.Name == "MainForm").FirstOrDefault();
            MainForm mf = (MainForm)frm;
            mf.setAlert(message);
        }
    }
}
