﻿using EntCorp.Data;
using EntCorp.Data.Models;
using System;
using System.Drawing;
using System.IO;
using DevExpress.XtraSplashScreen;

using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using EntCorp.Service;
using EntCorp.Utils;

namespace EntCorp.Helpers
{
    class FileUpload
    {
        public static string SavePhoto(Image photo, string subdir)
        {
            string directory = Path.Combine("photos", subdir);
            string path = String.Empty;
            try
            {
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                path = Path.Combine(directory, DateTime.Now.Ticks.ToString() + ".jpg");
                photo.Save(path);
            }
            catch (Exception ex)
            {
                Logger.LogWarn($"The process failed: {ex.ToString()}");
            }            
            return path;
        }

        public static bool ImportFromExcel(string filePath, DepartmentService departmentService, EmployeService employeService)
        {
            try
            {
                SplashScreenManager.ShowForm(typeof(SplashScreen1));
                EntCorpContext context = new EntCorpContext();
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(filePath);
                Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                Excel.Range xlRange = xlWorksheet.UsedRange;
                int rowCount = xlRange.Rows.Count;
                int depId = 0;
                for (int i = 1; i <= rowCount; i++)
                {
                    if (i > 3)
                    {
                        string tableNum = xlRange.Cells[i, 1] != null && xlRange.Cells[i, 1].Value != null ? xlRange.Cells[i, 1].Value.ToString() : "";
                        var fullname = xlRange.Cells[i, 2] != null && xlRange.Cells[i, 2].Value != null ? xlRange.Cells[i, 2].Value.ToString() : "";
                        var gender = xlRange.Cells[i, 3] != null && xlRange.Cells[i, 3].Value != null ? xlRange.Cells[i, 3].Value.ToString() : "";
                        var birthday = xlRange.Cells[i, 4] != null && xlRange.Cells[i, 4].Value != null ? xlRange.Cells[i, 4].Value.ToString() : "";
                        var code = xlRange.Cells[i, 5] != null && xlRange.Cells[i, 5].Value != null ? xlRange.Cells[i, 5].Value.ToString() : "";
                        var position = xlRange.Cells[i, 6] != null && xlRange.Cells[i, 6].Value != null ? xlRange.Cells[i, 6].Value.ToString() : "";
                        var note = xlRange.Cells[i, 7] != null && xlRange.Cells[i, 7].Value != null ? xlRange.Cells[i, 7].Value.ToString() : "";

                        string log = String.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}", tableNum, fullname, gender, birthday, code, position, note);
                        Logger.LogInfo(log);
                        Alerts.SetMessage(log);
                        if (!String.IsNullOrEmpty(tableNum) && String.IsNullOrEmpty(fullname))
                        {
                            Department d = departmentService.GetDepartmentByName(tableNum);
                            if (d == null)
                            {
                                d = new Department();
                                d.Name = tableNum;
                                d.Active = true;
                                departmentService.CreateDepartment(d);
                            }
                            depId = d.Id;
                        }

                        if (!String.IsNullOrEmpty(tableNum) && !String.IsNullOrEmpty(fullname))
                        {
                            Employe em = new Employe();
                            em.TableNumber = tableNum;
                            em.Fullname = fullname;

                            if (!String.IsNullOrEmpty(gender))
                            {
                                em.Gender = true;
                                if (gender[0] == 'ж' || gender[0] == 'Ж')
                                    em.Gender = false;
                            }

                            if (!String.IsNullOrEmpty(birthday))
                            {
                                em.Birthdate = Convert.ToDateTime(birthday);
                            }
                            else
                            {
                                em.Birthdate = null;
                            }

                            em.Code = code;
                            em.Position = position;
                            em.DepartmentId = depId;
                            employeService.CreateEmploye(em);
                        }
                    }
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                SplashScreenManager.CloseForm();
                return true;
            }
            catch (Exception ex)
            {
                SplashScreenManager.CloseForm();
                Alerts.SetMessage(ex.Message);
                Logger.LogError(ex.Message);
                return false;
            }
        }
    }
}