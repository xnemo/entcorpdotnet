﻿using System;
using System.Windows.Forms;
using EntCorp.Data.Models;
using EntCorp.Helpers;
using EntCorp.Repository;
using EntCorp.Service;
using EntCorp.Utils;

namespace EntCorp.Forms
{
    public partial class AdminForm : Form
    {
        private DeviceService _deviceService;
        private VisitService _visitService;
        private EmployeService _employeService;
        private UserService _userService;

        public AdminForm(IRepositoryWrapper repoWrapper)
        {
            InitializeComponent();
            _deviceService = new DeviceService(repoWrapper);
            _visitService = new VisitService(repoWrapper);
            _employeService = new EmployeService(repoWrapper);
            _userService = new UserService(repoWrapper);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            LoadDevices();
            LoadUsers();
        }

        //Load Devices
        private void LoadDevices()
        {
            var devices = _deviceService.GetAllDevices();
            deviceBindingSource.DataSource = devices;
            deviceBindingSource.ResetBindings(false);
            deviceGridControl.Invalidate();
        }

        //Add Device Form
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Device device = new Device();
            DeviceForm df = new DeviceForm { Device = device };
            if (df.ShowDialog() == DialogResult.OK)
            {
                _deviceService.CreateDevice(device);
                LoadDevices();
            }
        }

        //Edit Device Form
        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!(gridView1.GetFocusedRow() is Device cr))
                return;

            try
            {
                DeviceForm df = new DeviceForm {Device = cr};
                if (df.ShowDialog() == DialogResult.OK)
                {
                    _deviceService.UpdateDevice(cr);
                    LoadDevices();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        //Delete Device
        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!(gridView1.GetFocusedRow() is Device cr))
                return;

            try
            {
                ConfirmDeleteForm df = new ConfirmDeleteForm();

                if (df.ShowDialog() == DialogResult.OK)
                {
                    _deviceService.RemoveDevice(cr);                    
                    LoadDevices();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        //Load Users
        private void LoadUsers()
        {
            var users = _userService.GetAllUsers();
            userBindingSource.DataSource = users;
            userBindingSource.ResetBindings(false);
            userGridControl.Invalidate();
        }

        //Add User Form
        private void addUserButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            UserForm uf = new UserForm(_userService) { User = new User() };
            if (uf.ShowDialog() == DialogResult.OK)
            {
                LoadUsers();
            }
            
        }

        //Edit User Form
        private void editUserButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!(gridView2.GetFocusedRow() is User user))
                return;

            try
            {
                UserForm uf = new UserForm(_userService) {User = user};
                if (uf.ShowDialog() == DialogResult.OK)
                {
                    LoadUsers();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        //Delete User
        private void deleteUserButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!(gridView2.GetFocusedRow() is User user))
                return;

            try
            {
                ConfirmDeleteForm df = new ConfirmDeleteForm();
                if (df.ShowDialog() == DialogResult.OK)
                {
                    _userService.RemoveUser(user);

                    LoadUsers();
                }
            }   
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            tabControl1.TabPages.RemoveAt(tabControl1.TabPages.IndexOf(usersPage));
            tabControl1.TabPages.RemoveAt(tabControl1.TabPages.IndexOf(settingsPage));
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (!(gridView1.GetFocusedRow() is Device cr))
                return;
            Clipboard.SetText(cr.Token.ToString());

            Alerts.SetMessage("Токен скопирован в буфер обмена");
        }
    }
}
