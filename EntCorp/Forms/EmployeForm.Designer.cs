﻿namespace EntCorp.Forms
{
    partial class EmployeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fullnameLabel;
            System.Windows.Forms.Label departmentLabel;
            System.Windows.Forms.Label genderLabel;
            System.Windows.Forms.Label birthdateLabel;
            System.Windows.Forms.Label positionLabel;
            System.Windows.Forms.Label rankLabel;
            System.Windows.Forms.Label codeLabel;
            System.Windows.Forms.Label tableNumberLabel;
            System.Windows.Forms.Label passportNumberLabel;
            System.Windows.Forms.Label passportDataLabel;
            System.Windows.Forms.Label addressLabel;
            System.Windows.Forms.Label phoneLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmployeForm));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.passDataField = new System.Windows.Forms.TextBox();
            this.employeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.passNumField = new System.Windows.Forms.TextBox();
            this.phoneField = new System.Windows.Forms.TextBox();
            this.addressField = new System.Windows.Forms.TextBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.photoField = new DevExpress.XtraEditors.PictureEdit();
            this.fullnameField = new System.Windows.Forms.TextBox();
            this.genderField = new System.Windows.Forms.ComboBox();
            this.rankField = new System.Windows.Forms.NumericUpDown();
            this.tableNumField = new System.Windows.Forms.TextBox();
            this.codeField = new System.Windows.Forms.TextBox();
            this.orderNumField = new System.Windows.Forms.TextBox();
            this.workScheduleField = new DevExpress.XtraEditors.LookUpEdit();
            this.workScheduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bdateField = new System.Windows.Forms.MaskedTextBox();
            this.hiringDateField = new System.Windows.Forms.MaskedTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.departmentLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.departmentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.positionField = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.codeDeviceText = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.devicesCombo = new System.Windows.Forms.ComboBox();
            this.deviceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.admissionsGridView = new System.Windows.Forms.DataGridView();
            this.deviceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tokenDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.employeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createdAtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updatedAtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deletedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.admissionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            fullnameLabel = new System.Windows.Forms.Label();
            departmentLabel = new System.Windows.Forms.Label();
            genderLabel = new System.Windows.Forms.Label();
            birthdateLabel = new System.Windows.Forms.Label();
            positionLabel = new System.Windows.Forms.Label();
            rankLabel = new System.Windows.Forms.Label();
            codeLabel = new System.Windows.Forms.Label();
            tableNumberLabel = new System.Windows.Forms.Label();
            passportNumberLabel = new System.Windows.Forms.Label();
            passportDataLabel = new System.Windows.Forms.Label();
            addressLabel = new System.Windows.Forms.Label();
            phoneLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.photoField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rankField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workScheduleField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workScheduleBindingSource)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.departmentLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentsBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deviceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.admissionsGridView)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.admissionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // fullnameLabel
            // 
            fullnameLabel.AutoSize = true;
            fullnameLabel.Location = new System.Drawing.Point(186, 15);
            fullnameLabel.Name = "fullnameLabel";
            fullnameLabel.Size = new System.Drawing.Size(46, 13);
            fullnameLabel.TabIndex = 2;
            fullnameLabel.Text = "Ф.И.О.:";
            // 
            // departmentLabel
            // 
            departmentLabel.AutoSize = true;
            departmentLabel.Location = new System.Drawing.Point(147, 83);
            departmentLabel.Name = "departmentLabel";
            departmentLabel.Size = new System.Drawing.Size(90, 13);
            departmentLabel.TabIndex = 4;
            departmentLabel.Text = "Подразделение:";
            // 
            // genderLabel
            // 
            genderLabel.AutoSize = true;
            genderLabel.Location = new System.Drawing.Point(202, 51);
            genderLabel.Name = "genderLabel";
            genderLabel.Size = new System.Drawing.Size(30, 13);
            genderLabel.TabIndex = 6;
            genderLabel.Text = "Пол:";
            // 
            // birthdateLabel
            // 
            birthdateLabel.AutoSize = true;
            birthdateLabel.Location = new System.Drawing.Point(329, 51);
            birthdateLabel.Name = "birthdateLabel";
            birthdateLabel.Size = new System.Drawing.Size(89, 13);
            birthdateLabel.TabIndex = 8;
            birthdateLabel.Text = "Дата рождения:";
            // 
            // positionLabel
            // 
            positionLabel.AutoSize = true;
            positionLabel.Location = new System.Drawing.Point(166, 115);
            positionLabel.Name = "positionLabel";
            positionLabel.Size = new System.Drawing.Size(68, 13);
            positionLabel.TabIndex = 12;
            positionLabel.Text = "Должность:";
            // 
            // rankLabel
            // 
            rankLabel.AutoSize = true;
            rankLabel.Location = new System.Drawing.Point(526, 147);
            rankLabel.Name = "rankLabel";
            rankLabel.Size = new System.Drawing.Size(47, 13);
            rankLabel.TabIndex = 14;
            rankLabel.Text = "Разряд:";
            // 
            // codeLabel
            // 
            codeLabel.AutoSize = true;
            codeLabel.Location = new System.Drawing.Point(544, 51);
            codeLabel.Name = "codeLabel";
            codeLabel.Size = new System.Drawing.Size(29, 13);
            codeLabel.TabIndex = 16;
            codeLabel.Text = "Код:";
            // 
            // tableNumberLabel
            // 
            tableNumberLabel.AutoSize = true;
            tableNumberLabel.Location = new System.Drawing.Point(147, 148);
            tableNumberLabel.Name = "tableNumberLabel";
            tableNumberLabel.Size = new System.Drawing.Size(91, 13);
            tableNumberLabel.TabIndex = 19;
            tableNumberLabel.Text = "Номер в табеле:";
            // 
            // passportNumberLabel
            // 
            passportNumberLabel.AutoSize = true;
            passportNumberLabel.Location = new System.Drawing.Point(19, 52);
            passportNumberLabel.Name = "passportNumberLabel";
            passportNumberLabel.Size = new System.Drawing.Size(94, 13);
            passportNumberLabel.TabIndex = 0;
            passportNumberLabel.Text = "Номер паспорта:";
            // 
            // passportDataLabel
            // 
            passportDataLabel.AutoSize = true;
            passportDataLabel.Location = new System.Drawing.Point(6, 81);
            passportDataLabel.Name = "passportDataLabel";
            passportDataLabel.Size = new System.Drawing.Size(107, 13);
            passportDataLabel.TabIndex = 2;
            passportDataLabel.Text = "Когда и кем выдан:";
            // 
            // addressLabel
            // 
            addressLabel.AutoSize = true;
            addressLabel.Location = new System.Drawing.Point(72, 22);
            addressLabel.Name = "addressLabel";
            addressLabel.Size = new System.Drawing.Size(41, 13);
            addressLabel.TabIndex = 4;
            addressLabel.Text = "Адрес:";
            // 
            // phoneLabel
            // 
            phoneLabel.AutoSize = true;
            phoneLabel.Location = new System.Drawing.Point(433, 23);
            phoneLabel.Name = "phoneLabel";
            phoneLabel.Size = new System.Drawing.Size(55, 13);
            phoneLabel.TabIndex = 6;
            phoneLabel.Text = "Телефон:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(441, 184);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(138, 13);
            label1.TabIndex = 34;
            label1.Text = "Дата принятия на работу:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(147, 184);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(89, 13);
            label2.TabIndex = 35;
            label2.Text = "Номер приказа:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(147, 218);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(88, 13);
            label3.TabIndex = 39;
            label3.Text = "График работы:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.passDataField);
            this.groupBox2.Controls.Add(this.passNumField);
            this.groupBox2.Controls.Add(this.phoneField);
            this.groupBox2.Controls.Add(this.addressField);
            this.groupBox2.Controls.Add(phoneLabel);
            this.groupBox2.Controls.Add(addressLabel);
            this.groupBox2.Controls.Add(passportDataLabel);
            this.groupBox2.Controls.Add(passportNumberLabel);
            this.groupBox2.Location = new System.Drawing.Point(6, 238);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(693, 113);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Адрес и паспортные данные";
            // 
            // passDataField
            // 
            this.passDataField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "PassportData", true));
            this.passDataField.Location = new System.Drawing.Point(119, 81);
            this.passDataField.Name = "passDataField";
            this.passDataField.Size = new System.Drawing.Size(568, 20);
            this.passDataField.TabIndex = 16;
            // 
            // employeBindingSource
            // 
            this.employeBindingSource.DataSource = typeof(EntCorp.Data.Models.Employe);
            // 
            // passNumField
            // 
            this.passNumField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "PassportNumber", true));
            this.passNumField.Location = new System.Drawing.Point(119, 52);
            this.passNumField.Name = "passNumField";
            this.passNumField.Size = new System.Drawing.Size(568, 20);
            this.passNumField.TabIndex = 15;
            // 
            // phoneField
            // 
            this.phoneField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "Phone", true));
            this.phoneField.Location = new System.Drawing.Point(494, 19);
            this.phoneField.Name = "phoneField";
            this.phoneField.Size = new System.Drawing.Size(193, 20);
            this.phoneField.TabIndex = 14;
            // 
            // addressField
            // 
            this.addressField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "Address", true));
            this.addressField.Location = new System.Drawing.Point(119, 19);
            this.addressField.Name = "addressField";
            this.addressField.Size = new System.Drawing.Size(308, 20);
            this.addressField.TabIndex = 13;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(602, 396);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(100, 28);
            this.simpleButton1.TabIndex = 17;
            this.simpleButton1.Text = "Сохранить";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "False",
            "True"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // photoField
            // 
            this.photoField.Location = new System.Drawing.Point(6, 6);
            this.photoField.Name = "photoField";
            this.photoField.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.photoField.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.photoField.Size = new System.Drawing.Size(135, 165);
            this.photoField.TabIndex = 24;
            // 
            // fullnameField
            // 
            this.fullnameField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "Fullname", true));
            this.fullnameField.Location = new System.Drawing.Point(240, 12);
            this.fullnameField.Name = "fullnameField";
            this.fullnameField.Size = new System.Drawing.Size(459, 20);
            this.fullnameField.TabIndex = 1;
            // 
            // genderField
            // 
            this.genderField.CausesValidation = false;
            this.genderField.FormattingEnabled = true;
            this.genderField.Items.AddRange(new object[] {
            "М",
            "Ж"});
            this.genderField.Location = new System.Drawing.Point(240, 47);
            this.genderField.Name = "genderField";
            this.genderField.Size = new System.Drawing.Size(81, 21);
            this.genderField.TabIndex = 2;
            // 
            // rankField
            // 
            this.rankField.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.employeBindingSource, "Rank", true));
            this.rankField.Location = new System.Drawing.Point(576, 145);
            this.rankField.Name = "rankField";
            this.rankField.Size = new System.Drawing.Size(123, 20);
            this.rankField.TabIndex = 8;
            // 
            // tableNumField
            // 
            this.tableNumField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "TableNumber", true));
            this.tableNumField.Location = new System.Drawing.Point(240, 145);
            this.tableNumField.Name = "tableNumField";
            this.tableNumField.Size = new System.Drawing.Size(178, 20);
            this.tableNumField.TabIndex = 7;
            // 
            // codeField
            // 
            this.codeField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "Code", true));
            this.codeField.Location = new System.Drawing.Point(576, 47);
            this.codeField.Name = "codeField";
            this.codeField.Size = new System.Drawing.Size(123, 20);
            this.codeField.TabIndex = 4;
            // 
            // orderNumField
            // 
            this.orderNumField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "RecruitmentOrder", true));
            this.orderNumField.Location = new System.Drawing.Point(240, 181);
            this.orderNumField.Name = "orderNumField";
            this.orderNumField.Size = new System.Drawing.Size(178, 20);
            this.orderNumField.TabIndex = 9;
            // 
            // workScheduleField
            // 
            this.workScheduleField.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.employeBindingSource, "WorkScheduleId", true));
            this.workScheduleField.Location = new System.Drawing.Point(240, 215);
            this.workScheduleField.Name = "workScheduleField";
            this.workScheduleField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.workScheduleField.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Название", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Type", "Тип", 34, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Default", "По умолчанию?", 45, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.workScheduleField.Properties.DataSource = this.workScheduleBindingSource;
            this.workScheduleField.Properties.DisplayMember = "Name";
            this.workScheduleField.Properties.ValueMember = "Id";
            this.workScheduleField.Size = new System.Drawing.Size(459, 20);
            this.workScheduleField.TabIndex = 11;
            // 
            // workScheduleBindingSource
            // 
            this.workScheduleBindingSource.DataSource = typeof(EntCorp.Data.Models.WorkSchedule);
            // 
            // bdateField
            // 
            this.bdateField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "Birthdate", true));
            this.bdateField.Location = new System.Drawing.Point(422, 47);
            this.bdateField.Mask = "00.00.0000";
            this.bdateField.Name = "bdateField";
            this.bdateField.Size = new System.Drawing.Size(114, 20);
            this.bdateField.TabIndex = 3;
            // 
            // hiringDateField
            // 
            this.hiringDateField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "HiringDate", true));
            this.hiringDateField.Location = new System.Drawing.Point(585, 181);
            this.hiringDateField.Mask = "00.00.0000";
            this.hiringDateField.Name = "hiringDateField";
            this.hiringDateField.Size = new System.Drawing.Size(114, 20);
            this.hiringDateField.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(713, 383);
            this.tabControl1.TabIndex = 42;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.departmentLookUpEdit);
            this.tabPage1.Controls.Add(this.positionField);
            this.tabPage1.Controls.Add(this.photoField);
            this.tabPage1.Controls.Add(this.hiringDateField);
            this.tabPage1.Controls.Add(fullnameLabel);
            this.tabPage1.Controls.Add(this.bdateField);
            this.tabPage1.Controls.Add(departmentLabel);
            this.tabPage1.Controls.Add(label3);
            this.tabPage1.Controls.Add(genderLabel);
            this.tabPage1.Controls.Add(this.workScheduleField);
            this.tabPage1.Controls.Add(birthdateLabel);
            this.tabPage1.Controls.Add(this.orderNumField);
            this.tabPage1.Controls.Add(positionLabel);
            this.tabPage1.Controls.Add(label2);
            this.tabPage1.Controls.Add(rankLabel);
            this.tabPage1.Controls.Add(label1);
            this.tabPage1.Controls.Add(codeLabel);
            this.tabPage1.Controls.Add(this.codeField);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.tableNumField);
            this.tabPage1.Controls.Add(tableNumberLabel);
            this.tabPage1.Controls.Add(this.fullnameField);
            this.tabPage1.Controls.Add(this.genderField);
            this.tabPage1.Controls.Add(this.rankField);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(705, 357);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Информация";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // departmentLookUpEdit
            // 
            this.departmentLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.employeBindingSource, "DepartmentId", true));
            this.departmentLookUpEdit.Location = new System.Drawing.Point(240, 80);
            this.departmentLookUpEdit.Name = "departmentLookUpEdit";
            this.departmentLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.departmentLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.departmentLookUpEdit.Properties.DataSource = this.departmentsBindingSource;
            this.departmentLookUpEdit.Properties.DisplayMember = "Name";
            this.departmentLookUpEdit.Properties.ValueMember = "Id";
            this.departmentLookUpEdit.Size = new System.Drawing.Size(459, 20);
            this.departmentLookUpEdit.TabIndex = 40;
            // 
            // departmentsBindingSource
            // 
            this.departmentsBindingSource.DataSource = typeof(EntCorp.Data.Models.Department);
            // 
            // positionField
            // 
            this.positionField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeBindingSource, "Position", true));
            this.positionField.Location = new System.Drawing.Point(240, 112);
            this.positionField.Name = "positionField";
            this.positionField.Size = new System.Drawing.Size(459, 20);
            this.positionField.TabIndex = 6;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.codeDeviceText);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.devicesCombo);
            this.tabPage2.Controls.Add(this.admissionsGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(705, 357);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Доступ и разрешение";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(345, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Код:";
            // 
            // codeDeviceText
            // 
            this.codeDeviceText.Location = new System.Drawing.Point(380, 14);
            this.codeDeviceText.Name = "codeDeviceText";
            this.codeDeviceText.Size = new System.Drawing.Size(188, 20);
            this.codeDeviceText.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(575, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Устройство:";
            // 
            // devicesCombo
            // 
            this.devicesCombo.DataSource = this.deviceBindingSource;
            this.devicesCombo.DisplayMember = "Name";
            this.devicesCombo.FormattingEnabled = true;
            this.devicesCombo.Location = new System.Drawing.Point(79, 14);
            this.devicesCombo.Name = "devicesCombo";
            this.devicesCombo.Size = new System.Drawing.Size(259, 21);
            this.devicesCombo.TabIndex = 1;
            // 
            // deviceBindingSource
            // 
            this.deviceBindingSource.DataSource = typeof(EntCorp.Data.Models.Device);
            // 
            // admissionsGridView
            // 
            this.admissionsGridView.AllowUserToAddRows = false;
            this.admissionsGridView.AllowUserToDeleteRows = false;
            this.admissionsGridView.AutoGenerateColumns = false;
            this.admissionsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.admissionsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.deviceDataGridViewTextBoxColumn,
            this.tokenDataGridViewTextBoxColumn,
            this.employeDataGridViewTextBoxColumn,
            this.idDataGridViewTextBoxColumn,
            this.createdAtDataGridViewTextBoxColumn,
            this.updatedAtDataGridViewTextBoxColumn,
            this.deletedDataGridViewTextBoxColumn});
            this.admissionsGridView.ContextMenuStrip = this.contextMenuStrip1;
            this.admissionsGridView.DataSource = this.admissionBindingSource;
            this.admissionsGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.admissionsGridView.Location = new System.Drawing.Point(3, 52);
            this.admissionsGridView.MultiSelect = false;
            this.admissionsGridView.Name = "admissionsGridView";
            this.admissionsGridView.ReadOnly = true;
            this.admissionsGridView.RowHeadersVisible = false;
            this.admissionsGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.admissionsGridView.Size = new System.Drawing.Size(699, 302);
            this.admissionsGridView.TabIndex = 0;
            // 
            // deviceDataGridViewTextBoxColumn
            // 
            this.deviceDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.deviceDataGridViewTextBoxColumn.DataPropertyName = "Device";
            this.deviceDataGridViewTextBoxColumn.HeaderText = "Устройство";
            this.deviceDataGridViewTextBoxColumn.Name = "deviceDataGridViewTextBoxColumn";
            this.deviceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tokenDataGridViewTextBoxColumn
            // 
            this.tokenDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tokenDataGridViewTextBoxColumn.DataPropertyName = "Token";
            this.tokenDataGridViewTextBoxColumn.HeaderText = "Токен (RFID)";
            this.tokenDataGridViewTextBoxColumn.Name = "tokenDataGridViewTextBoxColumn";
            this.tokenDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // employeDataGridViewTextBoxColumn
            // 
            this.employeDataGridViewTextBoxColumn.DataPropertyName = "Employe";
            this.employeDataGridViewTextBoxColumn.HeaderText = "Employe";
            this.employeDataGridViewTextBoxColumn.Name = "employeDataGridViewTextBoxColumn";
            this.employeDataGridViewTextBoxColumn.ReadOnly = true;
            this.employeDataGridViewTextBoxColumn.Visible = false;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // createdAtDataGridViewTextBoxColumn
            // 
            this.createdAtDataGridViewTextBoxColumn.DataPropertyName = "CreatedAt";
            this.createdAtDataGridViewTextBoxColumn.HeaderText = "CreatedAt";
            this.createdAtDataGridViewTextBoxColumn.Name = "createdAtDataGridViewTextBoxColumn";
            this.createdAtDataGridViewTextBoxColumn.ReadOnly = true;
            this.createdAtDataGridViewTextBoxColumn.Visible = false;
            // 
            // updatedAtDataGridViewTextBoxColumn
            // 
            this.updatedAtDataGridViewTextBoxColumn.DataPropertyName = "UpdatedAt";
            this.updatedAtDataGridViewTextBoxColumn.HeaderText = "UpdatedAt";
            this.updatedAtDataGridViewTextBoxColumn.Name = "updatedAtDataGridViewTextBoxColumn";
            this.updatedAtDataGridViewTextBoxColumn.ReadOnly = true;
            this.updatedAtDataGridViewTextBoxColumn.Visible = false;
            // 
            // deletedDataGridViewTextBoxColumn
            // 
            this.deletedDataGridViewTextBoxColumn.DataPropertyName = "Deleted";
            this.deletedDataGridViewTextBoxColumn.HeaderText = "Deleted";
            this.deletedDataGridViewTextBoxColumn.Name = "deletedDataGridViewTextBoxColumn";
            this.deletedDataGridViewTextBoxColumn.ReadOnly = true;
            this.deletedDataGridViewTextBoxColumn.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалитьToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(119, 26);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // admissionBindingSource
            // 
            this.admissionBindingSource.DataSource = typeof(EntCorp.Data.Models.Admission);
            // 
            // EmployeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 437);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.simpleButton1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EmployeForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Сотрудник";
            this.Load += new System.EventHandler(this.EmployeForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.photoField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rankField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workScheduleField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workScheduleBindingSource)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.departmentLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentsBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deviceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.admissionsGridView)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.admissionBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.PictureEdit photoField;
        private System.Windows.Forms.TextBox fullnameField;
        private System.Windows.Forms.ComboBox genderField;
        private System.Windows.Forms.NumericUpDown rankField;
        private System.Windows.Forms.TextBox passDataField;
        private System.Windows.Forms.TextBox passNumField;
        private System.Windows.Forms.TextBox phoneField;
        private System.Windows.Forms.TextBox addressField;
        private System.Windows.Forms.TextBox tableNumField;
        private System.Windows.Forms.TextBox codeField;
        private System.Windows.Forms.TextBox orderNumField;
        private DevExpress.XtraEditors.LookUpEdit workScheduleField;
        private System.Windows.Forms.BindingSource workScheduleBindingSource;
        private System.Windows.Forms.MaskedTextBox bdateField;
        private System.Windows.Forms.MaskedTextBox hiringDateField;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView admissionsGridView;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox codeDeviceText;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox devicesCombo;
        private System.Windows.Forms.BindingSource deviceBindingSource;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.TextBox positionField;
        private System.Windows.Forms.BindingSource employeBindingSource;
        private System.Windows.Forms.BindingSource admissionBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn deviceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tokenDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn employeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdAtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn updatedAtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deletedDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource departmentsBindingSource;
        private DevExpress.XtraEditors.LookUpEdit departmentLookUpEdit;
    }
}