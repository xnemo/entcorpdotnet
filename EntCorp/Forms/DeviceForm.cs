﻿using EntCorp.Data;
using EntCorp.Data.Models;
using System;
//using System.Data.Entity;
using System.Windows.Forms;

namespace EntCorp.Forms
{
    public partial class DeviceForm : Form
    {
        public Device Device { get; set; }
        public DeviceForm()
        {
            InitializeComponent();
        }
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            readerTypeComboBox.DataSource = Enum.GetValues(typeof(ReaderTypes));
            readerTypeComboBox.DisplayMember = "Value";
            readerTypeComboBox.DataBindings.Add("SelectedValue", bsDevice, "ReaderType", true, DataSourceUpdateMode.OnPropertyChanged);

            ActiveControl = nameField;
            bsDevice.DataSource = Device;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void bsDevice_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void readerTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            gateLabel.Visible = readerTypeComboBox.Text == "GATE";
            gateNumberField.Visible = readerTypeComboBox.Text == "GATE";
            ipLabel.Text = readerTypeComboBox.Text == "GATE" ? "COM-порт" : "IP-адрес:";
        }
    }
}
