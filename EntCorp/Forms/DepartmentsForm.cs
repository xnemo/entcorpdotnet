﻿using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using EntCorp.Data.Models;
using EntCorp.Helpers;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;
using System.Threading.Tasks;
using System.ComponentModel;
using EntCorp.Service;
using EntCorp.Repository;
using System.Collections.Generic;
using EntCorp.Utils;

namespace EntCorp.Forms
{
    public partial class DepartmentsForm : Form
    {
        bool isRunning = false;
        private EmployeService _employeService;
        private DepartmentService _departmentService;
        private WorkScheduleService _workScheduleService;
        private DeviceService _deviceService;
        private AdmissionService _admissionService;
        private SyncDevicesService _syncService;
        private IEnumerable<Department> departments;
        private IEnumerable<WorkSchedule> workSchedules;
        private IEnumerable<Device> devices;

        public DepartmentsForm(IRepositoryWrapper repoWrapper)
        {
            InitializeComponent();
            _employeService = new EmployeService(repoWrapper);
            _departmentService = new DepartmentService(repoWrapper);
            _workScheduleService = new WorkScheduleService(repoWrapper);
            _deviceService = new DeviceService(repoWrapper);
            _admissionService = new AdmissionService(repoWrapper);
            _syncService = new SyncDevicesService(repoWrapper);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);           
            LoadDepartments();
            LoadEmploes();
            workSchedules = _workScheduleService.GetAllWorkSchedules();
            devices = _deviceService.GetAllDevices();
        }
        
        private void LoadDepartments()
        {
            departments = _departmentService.GetAllDepartmentsWithWorkSchedule();
            departmentBindingSource.DataSource = departments;
            departmentBindingSource.ResetBindings(false);
            departmentGridControl.Invalidate();
        }

        private void LoadEmploes(int rowIndex = 0)
        {
            var employes = _employeService.GetAllEmployesWithDepartments();
            employeBindingSource.DataSource = employes;
            employeBindingSource.ResetBindings(false);
            employeGridControl.Invalidate();

            RepositoryItemCheckEdit checkEdit = employeGridControl.RepositoryItems.Add("CheckEdit") as RepositoryItemCheckEdit;
            checkEdit.PictureChecked = Image.FromFile("img\\m.png");
            checkEdit.PictureUnchecked = Image.FromFile("img\\f.png");
            checkEdit.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            gridView1.Columns["Gender"].ColumnEdit = checkEdit;

            //int ind = gridView1.FocusedRowHandle;
            //gridView1.CollapseAllGroups();
            //gridView1.ExpandGroupRow(rowIndex);
        }

        //Edit Employee with DoubleClick on grid
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(ea.Location);

            if (info.InRow || info.InRowCell)
            {
                var Id = view.GetRowCellValue(info.RowHandle, "Id");
                if (Id != null)
                {
                    openEditForm();
                }
            }
        }

        //Visible || Hide columns
        private void barCheckItem1_CheckedChanged_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool isVisible = allFieldVisibleCheckbox.Checked;

            colAddress.Visible = isVisible;
            colPassportNumber.Visible = isVisible;
            colPassportData.Visible = isVisible;
            colRecruitmentOrder.Visible = isVisible;
            colHiringDate.Visible = isVisible;
        }

        //Add Employe form
        private void addEmployeButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Employe employe = new Employe();
                EmployeForm ef = new EmployeForm(departments, workSchedules, devices, _admissionService) { Employe = employe };
                if (ef.ShowDialog() != DialogResult.OK)
                    return;

                _employeService.CreateEmploye(employe);
                int rowIndex = gridView1.FocusedRowHandle;
                LoadEmploes(rowIndex);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
            
        }

        //Edit Employe Form
        private void editEmployeButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            openEditForm();
        }

        private void openEditForm()
        {
            if (!(gridView1.GetFocusedRow() is Employe employe))
                return;
            try
            {
                EmployeForm ef = new EmployeForm(departments, workSchedules, devices, _admissionService) { Employe = employe };
                if (ef.ShowDialog() != DialogResult.OK)
                    return;
                _employeService.UpdateEmploye(employe);
                LoadEmploes();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        //Delete Employe
        private void removeEmployeButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ConfirmDeleteForm df = new ConfirmDeleteForm();
                if (df.ShowDialog() != DialogResult.OK)
                    return;

                foreach (var row in gridView1.GetSelectedRows())
                {
                    if (!(gridView1.GetRow(row) is Employe employe))
                        return;
                    _employeService.RemoveEmploye(employe);
                    Alerts.SetMessage($"{employe.Fullname} перемещен(а) в список удаленных");
                }

                LoadEmploes();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Console.WriteLine(ex.Message);
            }
        }

        //Import Employe from Excel
        private void importEmployeButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            if (FileUpload.ImportFromExcel(openFileDialog1.FileName, _departmentService, _employeService))
            {
                importEmployeButton.Enabled = false;
                LoadEmploes();
                importEmployeButton.Enabled = true;
            }
        }

        //Print Employees List
        private void printEmployeButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridView1.ShowPrintPreview();
        }

        //Export Employees list to Excel
        private void exportEmployeButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string excelFileName = "EmployeList.xlsx";
            gridView1.ExportToXlsx(excelFileName);
            Process.Start(excelFileName);
        }

        //Add Department form
        private void addDepartmentButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var workSchedules = _workScheduleService.GetAllWorkSchedules();
            Department department = new Department();
            DepartmentForm df = new DepartmentForm(workSchedules) { Department = department };
            if (df.ShowDialog() != DialogResult.OK)
                return;
            _departmentService.CreateDepartment(department);
            LoadDepartments();
        }

        //Edit Department Form
        private void editDepartmentButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!(gridView2.GetFocusedRow() is Department department))
                return;

            try
            {
                var workSchedules = _workScheduleService.GetAllWorkSchedules();
                DepartmentForm df = new DepartmentForm(workSchedules) { Department = department };
                if (df.ShowDialog() != DialogResult.OK)
                    return;
                _departmentService.UpdateDepartment(department);
                LoadDepartments();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        //Delete Department
        private void deleteDepartmentButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ConfirmDeleteForm df = new ConfirmDeleteForm();
                if (df.ShowDialog() != DialogResult.OK)
                    return;

                if (!(gridView2.GetFocusedRow() is Department cr))
                    return;

                _departmentService.RemoveDepartment(cr);
                LoadDepartments();
                LoadEmploes();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                SetAdmissionForm saf = new SetAdmissionForm() { Devices = _deviceService.GetAllDevices() };
                if (saf.ShowDialog() == DialogResult.OK)
                {
                    SplashScreenManager.ShowForm(typeof(SplashScreen1));
                    foreach (var row in gridView1.GetSelectedRows())
                    {
                        if (!(gridView1.GetRow(row) is Employe cr))
                            continue;

                        Admission adm = new Admission()
                        {
                            Token = cr.Code,
                            EmployeId = cr.Id,
                            DeviceId = Convert.ToInt32(saf.DeviceId)   
                        };

                        _admissionService.CreateAdmission(adm);
                    }
                    SplashScreenManager.CloseForm();
                    gridView1.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        private async void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                barButtonItem2.Enabled = false;
                string result = await Task.Factory.StartNew<string>(
                    () => _syncService.SendData(), TaskCreationOptions.LongRunning);

                Alerts.SetMessage(result);
                barButtonItem2.Enabled = true;
            }
            catch (Exception ex)
            {
                Alerts.SetAlert(ex.Message);
                Logger.LogError(ex.Message);
            }
            
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Process.Start(@"Template.xlsx");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
        }
    }
}
