﻿using EntCorp.Contracts;
using EntCorp.Helpers;
using EntCorp.Service;
using System;
using System.Windows.Forms;

namespace EntCorp.Forms
{
    public partial class AuthForm : Form
    {
        private UserService _userService;

        public AuthForm()
        {
            InitializeComponent();
        }

        public AuthForm(IRepositoryWrapper repoWrapper)
        {
            InitializeComponent();
            _userService = new UserService(repoWrapper);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AuthService authService = new AuthService(_userService);
            bool auth = authService.Auth(fieldUsername.Text, fieldPassword.Text, out string result);
            Alerts.SetMessage(result);
            this.DialogResult = auth ? DialogResult.OK : DialogResult.None;
        }
    }
}
