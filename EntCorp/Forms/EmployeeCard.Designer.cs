﻿namespace EntCorp.Forms
{
    partial class EmployeeCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fullnameLabel;
            System.Windows.Forms.Label birthdateLabel;
            System.Windows.Forms.Label departmentLabel;
            System.Windows.Forms.Label positionLabel;
            System.Windows.Forms.Label tableNumberLabel;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label rankLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label phoneLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmployeeCard));
            this._orderDate = new System.Windows.Forms.Label();
            this._rank = new System.Windows.Forms.Label();
            this._orderNum = new System.Windows.Forms.Label();
            this._tableNum = new System.Windows.Forms.Label();
            this._phone = new System.Windows.Forms.Label();
            this._birthdate = new System.Windows.Forms.Label();
            this._position = new System.Windows.Forms.Label();
            this._department = new System.Windows.Forms.Label();
            this._fullname = new System.Windows.Forms.Label();
            this.photoField = new DevExpress.XtraEditors.PictureEdit();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.печатьToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.pageCount = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Photo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RegisteredAt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Event = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Device = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.employeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            fullnameLabel = new System.Windows.Forms.Label();
            birthdateLabel = new System.Windows.Forms.Label();
            departmentLabel = new System.Windows.Forms.Label();
            positionLabel = new System.Windows.Forms.Label();
            tableNumberLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            rankLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            phoneLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.photoField.Properties)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // fullnameLabel
            // 
            fullnameLabel.AutoSize = true;
            fullnameLabel.Location = new System.Drawing.Point(122, 11);
            fullnameLabel.Name = "fullnameLabel";
            fullnameLabel.Size = new System.Drawing.Size(46, 13);
            fullnameLabel.TabIndex = 3;
            fullnameLabel.Text = "Ф.И.О.:";
            // 
            // birthdateLabel
            // 
            birthdateLabel.AutoSize = true;
            birthdateLabel.Location = new System.Drawing.Point(122, 71);
            birthdateLabel.Name = "birthdateLabel";
            birthdateLabel.Size = new System.Drawing.Size(89, 13);
            birthdateLabel.TabIndex = 26;
            birthdateLabel.Text = "Дата рождения:";
            // 
            // departmentLabel
            // 
            departmentLabel.AutoSize = true;
            departmentLabel.Location = new System.Drawing.Point(122, 31);
            departmentLabel.Name = "departmentLabel";
            departmentLabel.Size = new System.Drawing.Size(90, 13);
            departmentLabel.TabIndex = 27;
            departmentLabel.Text = "Подразделение:";
            // 
            // positionLabel
            // 
            positionLabel.AutoSize = true;
            positionLabel.Location = new System.Drawing.Point(122, 51);
            positionLabel.Name = "positionLabel";
            positionLabel.Size = new System.Drawing.Size(68, 13);
            positionLabel.TabIndex = 28;
            positionLabel.Text = "Должность:";
            // 
            // tableNumberLabel
            // 
            tableNumberLabel.AutoSize = true;
            tableNumberLabel.Location = new System.Drawing.Point(122, 91);
            tableNumberLabel.Name = "tableNumberLabel";
            tableNumberLabel.Size = new System.Drawing.Size(91, 13);
            tableNumberLabel.TabIndex = 29;
            tableNumberLabel.Text = "Номер в табеле:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(122, 111);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(89, 13);
            label2.TabIndex = 36;
            label2.Text = "Номер приказа:";
            // 
            // rankLabel
            // 
            rankLabel.AutoSize = true;
            rankLabel.Location = new System.Drawing.Point(377, 91);
            rankLabel.Name = "rankLabel";
            rankLabel.Size = new System.Drawing.Size(47, 13);
            rankLabel.TabIndex = 37;
            rankLabel.Text = "Разряд:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(377, 111);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(138, 13);
            label1.TabIndex = 38;
            label1.Text = "Дата принятия на работу:";
            // 
            // phoneLabel
            // 
            phoneLabel.AutoSize = true;
            phoneLabel.Location = new System.Drawing.Point(377, 71);
            phoneLabel.Name = "phoneLabel";
            phoneLabel.Size = new System.Drawing.Size(55, 13);
            phoneLabel.TabIndex = 39;
            phoneLabel.Text = "Телефон:";
            // 
            // _orderDate
            // 
            this._orderDate.AutoSize = true;
            this._orderDate.Location = new System.Drawing.Point(521, 111);
            this._orderDate.Name = "_orderDate";
            this._orderDate.Size = new System.Drawing.Size(13, 13);
            this._orderDate.TabIndex = 48;
            this._orderDate.Text = "  ";
            // 
            // _rank
            // 
            this._rank.AutoSize = true;
            this._rank.Location = new System.Drawing.Point(430, 91);
            this._rank.Name = "_rank";
            this._rank.Size = new System.Drawing.Size(13, 13);
            this._rank.TabIndex = 47;
            this._rank.Text = "  ";
            // 
            // _orderNum
            // 
            this._orderNum.AutoSize = true;
            this._orderNum.Location = new System.Drawing.Point(217, 111);
            this._orderNum.Name = "_orderNum";
            this._orderNum.Size = new System.Drawing.Size(13, 13);
            this._orderNum.TabIndex = 46;
            this._orderNum.Text = "  ";
            // 
            // _tableNum
            // 
            this._tableNum.AutoSize = true;
            this._tableNum.Location = new System.Drawing.Point(219, 91);
            this._tableNum.Name = "_tableNum";
            this._tableNum.Size = new System.Drawing.Size(13, 13);
            this._tableNum.TabIndex = 45;
            this._tableNum.Text = "  ";
            // 
            // _phone
            // 
            this._phone.AutoSize = true;
            this._phone.Location = new System.Drawing.Point(438, 71);
            this._phone.Name = "_phone";
            this._phone.Size = new System.Drawing.Size(13, 13);
            this._phone.TabIndex = 44;
            this._phone.Text = "  ";
            // 
            // _birthdate
            // 
            this._birthdate.AutoSize = true;
            this._birthdate.Location = new System.Drawing.Point(217, 71);
            this._birthdate.Name = "_birthdate";
            this._birthdate.Size = new System.Drawing.Size(13, 13);
            this._birthdate.TabIndex = 43;
            this._birthdate.Text = "  ";
            // 
            // _position
            // 
            this._position.AutoSize = true;
            this._position.Location = new System.Drawing.Point(191, 51);
            this._position.Name = "_position";
            this._position.Size = new System.Drawing.Size(13, 13);
            this._position.TabIndex = 42;
            this._position.Text = "  ";
            // 
            // _department
            // 
            this._department.AutoSize = true;
            this._department.Location = new System.Drawing.Point(218, 31);
            this._department.Name = "_department";
            this._department.Size = new System.Drawing.Size(13, 13);
            this._department.TabIndex = 41;
            this._department.Text = "  ";
            // 
            // _fullname
            // 
            this._fullname.AutoSize = true;
            this._fullname.Location = new System.Drawing.Point(174, 11);
            this._fullname.Name = "_fullname";
            this._fullname.Size = new System.Drawing.Size(13, 13);
            this._fullname.TabIndex = 40;
            this._fullname.Text = "  ";
            // 
            // photoField
            // 
            this.photoField.Location = new System.Drawing.Point(10, 11);
            this.photoField.Name = "photoField";
            this.photoField.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.photoField.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.photoField.Size = new System.Drawing.Size(106, 123);
            this.photoField.TabIndex = 25;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.печатьToolStripButton,
            this.toolStripSeparator,
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.toolStripButton2,
            this.pageCount,
            this.toolStripButton3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1191, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // печатьToolStripButton
            // 
            this.печатьToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.печатьToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("печатьToolStripButton.Image")));
            this.печатьToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.печатьToolStripButton.Name = "печатьToolStripButton";
            this.печатьToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.печатьToolStripButton.Text = "&Печать";
            this.печатьToolStripButton.Click += new System.EventHandler(this.печатьToolStripButton_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Показать/Спрятать фотографию";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(58, 22);
            this.toolStripButton2.Text = "Пред.";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // pageCount
            // 
            this.pageCount.Name = "pageCount";
            this.pageCount.Size = new System.Drawing.Size(75, 25);
            this.pageCount.SelectedIndexChanged += new System.EventHandler(this.pageCount_SelectedIndexChanged);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(57, 22);
            this.toolStripButton3.Text = "След.";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 25);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1191, 507);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Photo,
            this.Id,
            this.Code,
            this.RegisteredAt,
            this.Event,
            this.Device});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 75;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridView1_CustomColumnDisplayText);
            // 
            // Photo
            // 
            this.Photo.Caption = "Фото";
            this.Photo.FieldName = "Photo";
            this.Photo.Name = "Photo";
            this.Photo.OptionsColumn.FixedWidth = true;
            this.Photo.Visible = true;
            this.Photo.VisibleIndex = 0;
            this.Photo.Width = 80;
            // 
            // Id
            // 
            this.Id.Caption = "Id";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            // 
            // Code
            // 
            this.Code.Caption = "Код";
            this.Code.FieldName = "Code";
            this.Code.Name = "Code";
            this.Code.Visible = true;
            this.Code.VisibleIndex = 1;
            this.Code.Width = 172;
            // 
            // RegisteredAt
            // 
            this.RegisteredAt.Caption = "Время прохода";
            this.RegisteredAt.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.RegisteredAt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.RegisteredAt.FieldName = "RegisteredAt";
            this.RegisteredAt.Name = "RegisteredAt";
            this.RegisteredAt.Visible = true;
            this.RegisteredAt.VisibleIndex = 2;
            this.RegisteredAt.Width = 172;
            // 
            // Event
            // 
            this.Event.Caption = "Событие";
            this.Event.FieldName = "Event";
            this.Event.Name = "Event";
            this.Event.Visible = true;
            this.Event.VisibleIndex = 3;
            this.Event.Width = 172;
            // 
            // Device
            // 
            this.Device.Caption = "Оборудование";
            this.Device.FieldName = "Device.Name";
            this.Device.Name = "Device";
            this.Device.Visible = true;
            this.Device.VisibleIndex = 4;
            this.Device.Width = 178;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this._orderDate);
            this.panel1.Controls.Add(this.photoField);
            this.panel1.Controls.Add(this._rank);
            this.panel1.Controls.Add(fullnameLabel);
            this.panel1.Controls.Add(this._orderNum);
            this.panel1.Controls.Add(birthdateLabel);
            this.panel1.Controls.Add(this._tableNum);
            this.panel1.Controls.Add(departmentLabel);
            this.panel1.Controls.Add(this._phone);
            this.panel1.Controls.Add(positionLabel);
            this.panel1.Controls.Add(this._birthdate);
            this.panel1.Controls.Add(tableNumberLabel);
            this.panel1.Controls.Add(this._position);
            this.panel1.Controls.Add(label2);
            this.panel1.Controls.Add(this._department);
            this.panel1.Controls.Add(rankLabel);
            this.panel1.Controls.Add(this._fullname);
            this.panel1.Controls.Add(label1);
            this.panel1.Controls.Add(phoneLabel);
            this.panel1.Location = new System.Drawing.Point(2, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1191, 155);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.gridControl1);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(2, 157);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1191, 532);
            this.panel2.TabIndex = 2;
            // 
            // employeBindingSource
            // 
            this.employeBindingSource.DataSource = typeof(EntCorp.Data.Models.Employe);
            // 
            // EmployeeCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1193, 690);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EmployeeCard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Карточка сотрудника";
            this.Load += new System.EventHandler(this.EmployeeCard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.photoField.Properties)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.PictureEdit photoField;
        private System.Windows.Forms.Label _fullname;
        private System.Windows.Forms.Label _orderNum;
        private System.Windows.Forms.Label _tableNum;
        private System.Windows.Forms.Label _phone;
        private System.Windows.Forms.Label _birthdate;
        private System.Windows.Forms.Label _position;
        private System.Windows.Forms.Label _department;
        private System.Windows.Forms.Label _rank;
        private System.Windows.Forms.Label _orderDate;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn Id;
        private DevExpress.XtraGrid.Columns.GridColumn Code;
        private DevExpress.XtraGrid.Columns.GridColumn RegisteredAt;
        private DevExpress.XtraGrid.Columns.GridColumn Event;
        private DevExpress.XtraGrid.Columns.GridColumn Device;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton печатьToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private DevExpress.XtraGrid.Columns.GridColumn Photo;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.BindingSource employeBindingSource;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripComboBox pageCount;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
    }
}