﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraVerticalGrid.Rows;
using EntCorp.Data.Models;
using EntCorp.Helpers;
using EntCorp.Repository;
using EntCorp.Service;
using EntCorp.Utils;

namespace EntCorp.Forms
{
    public partial class WorkScheduleForm : Form
    {
        WorkScheduleService _workScheduleService;
        WorkSchemaService _workSchemaService;
        HolidayService _holidayService;
        public WorkScheduleForm(IRepositoryWrapper repoWrapper)
        {
            InitializeComponent();
            _workScheduleService = new WorkScheduleService(repoWrapper);
            _workSchemaService = new WorkSchemaService(repoWrapper);
            _holidayService = new HolidayService(repoWrapper);
        }

        private void WorkScheduleForm_Load(object sender, EventArgs e)
        {
            LoadWorkSchedules();
            LoadHolydays();
        }

        private void LoadWorkSchedules()
        {
            var workSchedules = _workScheduleService.GetAllWorkSchedules();
            workScheduleBindingSource.DataSource = workSchedules;
        }

        private void LoadHolydays()
        {
            var holidays = _holidayService.GetAllHolidays();
            this.holidayBindingSource.DataSource = holidays;
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            try
            {
                if (!(e.Row is WorkSchedule w))
                    return;

                if (w.isNew())
                {
                    _workScheduleService.CreateWorkSchedule(w);
                }
                else
                {
                    _workScheduleService.UpdateWorkSchedule(w);
                }

                LoadWorkSchedules();
                workScheduleGridControl.Refresh();
                loadWorkTable();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert(ex.Message);
            }
            
        }
        
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            loadWorkTable();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                var scheduleId = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, colId);
                int weekday = 1;

                foreach (var row in vGridControl1.Rows)
                {
                    if (row != vGridControl1.Rows[0])
                    {
                        string startTime = row.GetRowProperties(0).Value == null ? "" : row.GetRowProperties(0).Value.ToString();
                        string endTime = row.GetRowProperties(1).Value == null ? "" : row.GetRowProperties(1).Value.ToString();
                        var hours = row.GetRowProperties(2).Value;

                        WorkSchema ws =
                            _workSchemaService.GetWorkSchemaByWorkScheduleAndDay((int)scheduleId, weekday)
                            ?? new WorkSchema();

                        if (String.IsNullOrEmpty(startTime) && String.IsNullOrEmpty(endTime))
                        {
                            if (!ws.isNew())
                                _workSchemaService.RemoveWorkSchema(ws);
                        }
                        else
                        {
                            ws.WorkScheduleId = (int)scheduleId;
                            ws.DayNumber = weekday;
                            ws.StartTime = startTime.ToString();
                            ws.EndTime = endTime.ToString();
                            ws.Hours = Convert.ToInt32(hours);

                            if (ws.isNew())
                            {
                                _workSchemaService.CreateWorkSchema(ws);
                            }
                            else
                            {
                                _workSchemaService.UpdateWorkSchema(ws);
                            }
                        }

                        weekday = weekday + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert(ex.Message);
            }
        }

        private void LoadSchema(int scheduleId, int type)
        {
            vGridControl1.Rows.Clear();

            var workSchemas = _workSchemaService.GetWorkSchemaBySchedule(scheduleId);

            string[] weekdays = { "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье" };

            if (type == 1)
            {
                vGridControl1.Rows.Add(CreateHeaderRow(1));

                int i = 1;
                foreach (var item in weekdays)
                {
                    MultiEditorRow meRow = new MultiEditorRow();
                    meRow.Name = String.Format("w{0}", i);
                    MultiEditorRowProperties meRowProperty = new MultiEditorRowProperties();
                    meRowProperty.FieldName = String.Format("startTime{0}", i);
                    meRowProperty.Caption = String.Format("{0}", item);
                    RepositoryItemTextEdit timeEdit = new RepositoryItemTextEdit();
                    timeEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
                    timeEdit.Mask.EditMask = "00:00";
                    meRowProperty.RowEdit = timeEdit;
                    meRow.PropertiesCollection.Add(meRowProperty);

                    MultiEditorRowProperties meRowProperty2 = new MultiEditorRowProperties();
                    meRowProperty2.FieldName = String.Format("endTime{0}", i);
                    RepositoryItemTextEdit timeEdit2 = new RepositoryItemTextEdit();
                    timeEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
                    timeEdit2.Mask.EditMask = "00:00";
                    meRowProperty2.RowEdit = timeEdit2;
                    meRow.PropertiesCollection.Add(meRowProperty2);

                    MultiEditorRowProperties meRowProperty3 = new MultiEditorRowProperties();
                    meRowProperty3.FieldName = String.Format("hours{0}", i);
                    meRowProperty3.RowEdit = new RepositoryItemSpinEdit();
                    meRow.PropertiesCollection.Add(meRowProperty3);

                    vGridControl1.Rows.Add(meRow);
                    i = i + 1;
                }
            }
            else
            {
                vGridControl1.Rows.Add(CreateHeaderRow(2));

                for (int i = 1; i < 31; i++)
                {
                    MultiEditorRow meRow = new MultiEditorRow();
                    meRow.Name = String.Format("w{0}", i);

                    MultiEditorRowProperties meRowProperty = new MultiEditorRowProperties();
                    meRowProperty.FieldName = String.Format("startTime{0}", i);
                    meRowProperty.Caption = String.Format("{0}-день", i);

                    RepositoryItemTextEdit timeEdit = new RepositoryItemTextEdit();
                    timeEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
                    timeEdit.Mask.EditMask = "00:00";
                    meRowProperty.RowEdit = timeEdit;
                    meRow.PropertiesCollection.Add(meRowProperty);

                    MultiEditorRowProperties meRowProperty2 = new MultiEditorRowProperties();
                    meRowProperty2.FieldName = String.Format("endTime{0}", i);
                    RepositoryItemTextEdit timeEdit2 = new RepositoryItemTextEdit();
                    timeEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
                    timeEdit2.Mask.EditMask = "00:00";
                    meRowProperty2.RowEdit = timeEdit2;
                    meRow.PropertiesCollection.Add(meRowProperty2);

                    MultiEditorRowProperties meRowProperty3 = new MultiEditorRowProperties();
                    meRowProperty3.FieldName = String.Format("hours{0}", i);
                    meRowProperty3.RowEdit = new RepositoryItemSpinEdit();
                    meRow.PropertiesCollection.Add(meRowProperty3);

                    vGridControl1.Rows.Add(meRow);
                }
            }

            foreach (var w in workSchemas)
            {
                var row = vGridControl1.GetRowByName(String.Format("w{0}", w.DayNumber));
                row.GetRowProperties(0).Value = w.StartTime;
                row.GetRowProperties(1).Value = w.EndTime;
                row.GetRowProperties(2).Value = w.Hours;

                Console.WriteLine(w.StartTime);
            }
        }

        private static MultiEditorRow CreateHeaderRow(int type)
        {
            MultiEditorRow headerRow = new MultiEditorRow();
            headerRow.Name = "headerRow";

            MultiEditorRowProperties headerProperty = new MultiEditorRowProperties();
            headerProperty.FieldName = "startTime";
            headerProperty.Caption = type==1 ? "День недели" : "День месяца";
            headerProperty.Value = "Начало";
            headerRow.PropertiesCollection.Add(headerProperty);
            MultiEditorRowProperties headerProperty2 = new MultiEditorRowProperties();
            headerProperty2.FieldName = "endTime";
            headerProperty2.Value = "Конец";
            headerRow.PropertiesCollection.Add(headerProperty2);

            MultiEditorRowProperties headerProperty3 = new MultiEditorRowProperties();
            headerProperty3.FieldName = "hours";
            headerProperty3.Value = "Перерив на обед";
            headerRow.PropertiesCollection.Add(headerProperty3);

            headerRow.Enabled = false;
            return headerRow;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(gridView2.GetFocusedRow() is Holiday holiday))
                    return;

                if (holiday.isNew())
                {
                    _holidayService.CreateHoliday(holiday);
                }
                else
                {
                    _holidayService.UpdateHoliday(holiday);
                }

                this.Validate();
                this.holidayGridControl.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert(ex.Message);
            }
            
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (!(gridView1.GetFocusedRow() is WorkSchedule w))
                    return;

                if (!w.isDefault())
                {
                    _workScheduleService.RemoveWorkSchedule(w);
                    this.Validate();
                }
                else
                {
                    Alerts.SetAlert("Нельзя удалить график работы по умолчанию");
                } 
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert(ex.Message);
            }

            this.LoadWorkSchedules();
            this.workScheduleGridControl.Refresh();
        }

        private void loadWorkTable(string value = null)
        {
            if (value == null)
            {
                value = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, colType).ToString();
            }
            
            int scheduleId = (int)gridView1.GetRowCellValue(gridView1.FocusedRowHandle, colId);

            switch (value)
            {
                case "Недельний":
                    schemaPanel.Visible = true;
                    this.LoadSchema(scheduleId, 1);
                    break;
                case "Скользящий":
                    schemaPanel.Visible = true;
                    this.LoadSchema(scheduleId, 0);
                    break;
                case "Свободный":
                    schemaPanel.Visible = false;
                    break;
            }
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            loadWorkTable();
        }

        private void gridView1_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.Name != "colType") return;
            loadWorkTable(e.Value.ToString());
        }

        private void bindingNavigatorDeleteItem1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(gridView2.GetFocusedRow() is Holiday holiday))
                    return;
                
                _holidayService.RemoveHoliday(holiday);

                this.Validate();
                this.LoadHolydays();
                this.holidayGridControl.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert(ex.Message);
            }
        }
    }
}
