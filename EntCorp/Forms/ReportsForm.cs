﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Printing;
using DevExpress.XtraGrid.Views.Grid;
using System.IO;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using EntCorp.Helpers;
using System.Threading.Tasks;
using EntCorp.Contracts;
using EntCorp.Service;
using EntCorp.Data.Models;
using System.Collections.Generic;
using System.Linq;
using EntCorp.Utils;

namespace EntCorp.Forms
{
    public partial class ReportsForm : Form
    {
        Timer timer = new Timer();
        EmployeService _employeService;
        WorkSchemaService _workSchemaServise;
        VisitService _visitService;
        SyncDevicesService _syncService;

        IEnumerable<Employe> _employes;
        IRepositoryWrapper _repoWrapper;

        public ReportsForm(IRepositoryWrapper repoWrapper)
        {
            InitializeComponent();
            _employeService = new EmployeService(repoWrapper);
            _workSchemaServise = new WorkSchemaService(repoWrapper);
            _visitService = new VisitService(repoWrapper);
            _syncService = new SyncDevicesService(repoWrapper);
            _repoWrapper = repoWrapper;
        }

        private void ReportsForm_Load(object sender, EventArgs e)
        {
            timer.Tick += new EventHandler(TimerEventProcessor);
            timer.Interval = 60 * 1000; 
            timer.Enabled = true;
            timer.Start();

            dateFilterBar.EditValue = DateTime.Now;
            LoadData();
        }

        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            LoadData();
        }

        private void dateFilterBar_EditValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private async void SyncData()
        {
            barButtonItem8.Enabled = false;
            var result = await _syncService.GetData();

            //Alerts.SetMessage(result.Result);
            //LoadData();
            barButtonItem8.Enabled = true;
        }

        public void LoadData()
        {
            try
            {
                DateTime date = Convert.ToDateTime(dateFilterBar.EditValue.ToString());
                string sdate = date.ToString("yyyy-MM-dd");

                List<DbRecord> records = new List<DbRecord>();
                _employes = _employeService.GetEmployeForReport(date.Date);

                foreach (var employe in _employes)
                {
                    Visit firstVisit = null, lastExit = null;

                    DbRecord record = new DbRecord()
                    {
                        Id = employe.Id,
                        Photo = employe.Photo,
                        Fullname = employe.Fullname,
                        Department = employe.Department.Name,
                        Position = employe.Position
                    };
                    
                    int dayNumber = 0;
                    switch (employe.WorkSchedule.Type)
                    {
                        case WorkSchedule.Types.Недельний:
                            dayNumber = (int)date.DayOfWeek;
                            break;
                        case WorkSchedule.Types.Скользящий:
                            dayNumber = date.Day;
                            break;
                    }

                    WorkSchema ws = _workSchemaServise.GetWorkSchemaByWorkScheduleAndDay(employe.WorkScheduleId, dayNumber);

                    var visits = _visitService.GetVisitsByPersonAndDate(employe.Id, date);
                    firstVisit = visits.FirstOrDefault(v => v.Event == Visit.Events.IN);
                    lastExit = visits.LastOrDefault(v => v.Event == Visit.Events.OUT);

                    record.FirstVisit = firstVisit?.RegisteredAt.ToString();
                    if (ws != null)
                    {
                        DateTime startTime = Convert.ToDateTime(sdate + " " + ws.StartTime);
                        record.IsLate = firstVisit?.RegisteredAt > startTime;
                    }

                    record.LastExit = lastExit?.RegisteredAt.ToString();
                    if (ws != null)
                    {
                        DateTime endTime = Convert.ToDateTime(sdate + " " + ws.EndTime);
                        record.IsEarlyExit = lastExit?.RegisteredAt < endTime;
                    }

                    record.IsAbsent = visits.Count() == 0;

                    records.Add(record);
                }
                gridControl1.DataSource = records;
            }
            catch (Exception ex)
            {
                Alerts.SetAlert(ex.Message);
                Logger.LogError(ex.Message);
            }
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            this.LoadPicture(sender, e);
        }

        private void LoadPicture(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            string directory = "photos";
            string photo = "noavatar.png";
            string picFilePath = Path.Combine(directory, photo);

            GridView currentView = sender as GridView;
            if (e.Column.FieldName == "Photo")
            {
                e.DisplayText = string.Empty;

                if (currentView.GetRowCellValue(e.RowHandle, currentView.Columns["Photo"]) != null)
                {
                    photo = currentView.GetRowCellValue(e.RowHandle, currentView.Columns["Photo"]).ToString();
                }

                if (File.Exists(photo))
                {
                    picFilePath = photo;
                }

                Image img = Image.FromFile(picFilePath);
                e.Graphics.DrawImage(img, e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height);
            }
        }

        private void gridView1_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            
        }

        private void barCheckItem1_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridView1.Columns["Photo"].Visible = barCheckItem1.Checked;
            gridView1.RowHeight = barCheckItem1.Checked ? 75 : -1;
        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridView1.ActiveFilter.Clear();
            gridView1.ActiveFilter.NonColumnFilter = "[IsAbsent] = true";
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridView1.ActiveFilter.Clear();
            gridView1.ActiveFilter.NonColumnFilter = "[IsLate] = true";
            
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridView1.ActiveFilter.Clear();
            gridView1.ActiveFilter.NonColumnFilter = "[IsEarlyExit] = true";
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridView1.ActiveFilter.Clear();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TabelForm tf = new TabelForm(_repoWrapper);
            tf.ShowDialog();
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //this.SyncData();
            LoadData();
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (!(gridView1.GetFocusedRow() is DbRecord record))
                    return;
                Employe employe = _employeService.GetEmployeById(record.Id);
                EmployeeCard ef = new EmployeeCard() { _employe = employe };
                ef.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert("Ошибка отображения карточки сотрудника");
            }
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CompositeLink compLink = getCompositeLink();
            compLink.ShowPreviewDialog();
        }

        private CompositeLink getCompositeLink()
        {
            barCheckItem1.Checked = false;
            CompositeLink compLink = new CompositeLink(new PrintingSystem());
            PrintableComponentLink gridLink = new PrintableComponentLink();
            gridLink.Component = gridControl1;
            compLink.PaperKind = PaperKind.A4;
            compLink.Landscape = true;
            compLink.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            compLink.Margins.Right = 5;
            compLink.Margins.Left = 5;
            compLink.Margins.Top = 5;
            compLink.Margins.Bottom = 5;
            Link headerLink = new Link();
            headerLink.CreateDetailArea += new CreateAreaEventHandler(headerLink_CreateDetailArea);
            compLink.Links.Add(headerLink);
            compLink.Links.Add(gridLink);

            return compLink;
        }

        void headerLink_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            DateTime date = Convert.ToDateTime(dateFilterBar.EditValue.ToString());
            string sdate = date.ToString("MM.dd.yyyy");

            e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Center);
            e.Graph.Font = new Font("Tahoma", 12, FontStyle.Bold);
            RectangleF rec = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 25);
            e.Graph.DrawString($"Посещение за  [{sdate}]", Color.Black, rec, BorderSide.None);
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string fileName = @"DayReport.xls";
            CompositeLink compLink = getCompositeLink();
            compLink.ExportToXls(fileName);
            Process.Start(fileName);
        }
    }
}
