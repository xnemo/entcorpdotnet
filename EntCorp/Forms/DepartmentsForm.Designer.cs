﻿namespace EntCorp.Forms
{
    partial class DepartmentsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DepartmentsForm));
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.departmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.employeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.departmentPage = new System.Windows.Forms.TabPage();
            this.ribbonControl2 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.addDepartmentButton = new DevExpress.XtraBars.BarButtonItem();
            this.editDepartmentButton = new DevExpress.XtraBars.BarButtonItem();
            this.deleteDepartmentButton = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.departmentGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkSchedule1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.workScheduleLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRank = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTableNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirthdate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkSchedule = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPassportNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPassportData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecruitmentOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHiringDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.employeGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.employePage = new System.Windows.Forms.TabPage();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.addEmployeButton = new DevExpress.XtraBars.BarButtonItem();
            this.editEmployeButton = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.printEmployeButton = new DevExpress.XtraBars.BarButtonItem();
            this.exportEmployeButton = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.allFieldVisibleCheckbox = new DevExpress.XtraBars.BarCheckItem();
            this.removeEmployeButton = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.importEmployeButton = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.employesPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeBindingSource)).BeginInit();
            this.departmentPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workScheduleLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.employePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // departmentBindingSource
            // 
            this.departmentBindingSource.DataSource = typeof(EntCorp.Data.Models.Department);
            this.departmentBindingSource.Filter = "";
            // 
            // employeBindingSource
            // 
            this.employeBindingSource.DataSource = typeof(EntCorp.Data.Models.Employe);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Excell|*.xls;*.xlsx;";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "Редактирование";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.ShowCaptionButton = false;
            this.ribbonPageGroup8.Text = "Редактирование";
            // 
            // departmentPage
            // 
            this.departmentPage.Controls.Add(this.ribbonControl2);
            this.departmentPage.Controls.Add(this.departmentGridControl);
            this.departmentPage.Location = new System.Drawing.Point(4, 22);
            this.departmentPage.Name = "departmentPage";
            this.departmentPage.Size = new System.Drawing.Size(983, 631);
            this.departmentPage.TabIndex = 1;
            this.departmentPage.Text = "Подразделения";
            this.departmentPage.UseVisualStyleBackColor = true;
            // 
            // ribbonControl2
            // 
            this.ribbonControl2.ExpandCollapseItem.Id = 0;
            this.ribbonControl2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl2.ExpandCollapseItem,
            this.addDepartmentButton,
            this.editDepartmentButton,
            this.deleteDepartmentButton});
            this.ribbonControl2.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl2.MaxItemId = 4;
            this.ribbonControl2.Name = "ribbonControl2";
            this.ribbonControl2.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl2.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl2.ShowCategoryInCaption = false;
            this.ribbonControl2.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl2.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbonControl2.Size = new System.Drawing.Size(983, 95);
            this.ribbonControl2.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // addDepartmentButton
            // 
            this.addDepartmentButton.Caption = "Новое подразделение";
            this.addDepartmentButton.Id = 1;
            this.addDepartmentButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("addDepartmentButton.ImageOptions.Image")));
            this.addDepartmentButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("addDepartmentButton.ImageOptions.LargeImage")));
            this.addDepartmentButton.Name = "addDepartmentButton";
            this.addDepartmentButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.addDepartmentButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addDepartmentButton_ItemClick);
            // 
            // editDepartmentButton
            // 
            this.editDepartmentButton.Caption = "Редактирование";
            this.editDepartmentButton.Id = 2;
            this.editDepartmentButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("editDepartmentButton.ImageOptions.Image")));
            this.editDepartmentButton.Name = "editDepartmentButton";
            this.editDepartmentButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.editDepartmentButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.editDepartmentButton_ItemClick);
            // 
            // deleteDepartmentButton
            // 
            this.deleteDepartmentButton.Caption = "Удаление";
            this.deleteDepartmentButton.Id = 3;
            this.deleteDepartmentButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("deleteDepartmentButton.ImageOptions.Image")));
            this.deleteDepartmentButton.Name = "deleteDepartmentButton";
            this.deleteDepartmentButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteDepartmentButton_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup6});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.addDepartmentButton);
            this.ribbonPageGroup6.ItemLinks.Add(this.editDepartmentButton);
            this.ribbonPageGroup6.ItemLinks.Add(this.deleteDepartmentButton);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Редактирование";
            // 
            // departmentGridControl
            // 
            this.departmentGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.departmentGridControl.DataSource = this.departmentBindingSource;
            this.departmentGridControl.Location = new System.Drawing.Point(-4, 92);
            this.departmentGridControl.MainView = this.gridView2;
            this.departmentGridControl.Name = "departmentGridControl";
            this.departmentGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox3,
            this.workScheduleLookUpEdit});
            this.departmentGridControl.Size = new System.Drawing.Size(991, 543);
            this.departmentGridControl.TabIndex = 2;
            this.departmentGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colActive1,
            this.colWorkSchedule1});
            this.gridView2.GridControl = this.departmentGridControl;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsEditForm.EditFormColumnCount = 1;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colName
            // 
            this.colName.Caption = "Название подразделения";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 272;
            // 
            // colActive1
            // 
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 1;
            this.colActive1.Width = 32;
            // 
            // colWorkSchedule1
            // 
            this.colWorkSchedule1.Caption = "Рабочий график";
            this.colWorkSchedule1.FieldName = "WorkSchedule.Name";
            this.colWorkSchedule1.Name = "colWorkSchedule1";
            this.colWorkSchedule1.Visible = true;
            this.colWorkSchedule1.VisibleIndex = 2;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // workScheduleLookUpEdit
            // 
            this.workScheduleLookUpEdit.AutoHeight = false;
            this.workScheduleLookUpEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.workScheduleLookUpEdit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Название графика", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Type", "Тип", 34, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Default", "По умолчанию", 45, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.workScheduleLookUpEdit.DisplayMember = "Name";
            this.workScheduleLookUpEdit.Name = "workScheduleLookUpEdit";
            this.workScheduleLookUpEdit.NullValuePromptShowForEmptyValue = true;
            this.workScheduleLookUpEdit.ShowNullValuePromptWhenFocused = true;
            this.workScheduleLookUpEdit.ValidateOnEnterKey = true;
            this.workScheduleLookUpEdit.ValueMember = "Id";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colGender,
            this.colFullname,
            this.colPosition,
            this.colRank,
            this.colTableNumber,
            this.colCode,
            this.colBirthdate,
            this.colPhone,
            this.colDepartment,
            this.colWorkSchedule,
            this.colAddress,
            this.colPassportNumber,
            this.colPassportData,
            this.colRecruitmentOrder,
            this.colHiringDate});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1351, 706, 210, 172);
            this.gridView1.GridControl = this.employeGridControl;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PreviewFieldName = "PassportData";
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepartment, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.Width = 41;
            // 
            // colGender
            // 
            this.colGender.Caption = " ";
            this.colGender.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGender.CustomizationCaption = " ";
            this.colGender.FieldName = "Gender";
            this.colGender.Name = "colGender";
            this.colGender.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.colGender.Visible = true;
            this.colGender.VisibleIndex = 1;
            this.colGender.Width = 41;
            // 
            // colFullname
            // 
            this.colFullname.Caption = "Имя, фамилия";
            this.colFullname.FieldName = "Fullname";
            this.colFullname.Name = "colFullname";
            this.colFullname.Visible = true;
            this.colFullname.VisibleIndex = 2;
            this.colFullname.Width = 64;
            // 
            // colPosition
            // 
            this.colPosition.Caption = "Должность";
            this.colPosition.FieldName = "Position";
            this.colPosition.Name = "colPosition";
            this.colPosition.Visible = true;
            this.colPosition.VisibleIndex = 3;
            this.colPosition.Width = 64;
            // 
            // colRank
            // 
            this.colRank.Caption = "Разряд";
            this.colRank.FieldName = "Rank";
            this.colRank.Name = "colRank";
            this.colRank.Visible = true;
            this.colRank.VisibleIndex = 4;
            this.colRank.Width = 21;
            // 
            // colTableNumber
            // 
            this.colTableNumber.Caption = "Табельный номер";
            this.colTableNumber.FieldName = "TableNumber";
            this.colTableNumber.Name = "colTableNumber";
            this.colTableNumber.Visible = true;
            this.colTableNumber.VisibleIndex = 5;
            this.colTableNumber.Width = 76;
            // 
            // colCode
            // 
            this.colCode.Caption = "КОД (РФИД)";
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            this.colCode.Visible = true;
            this.colCode.VisibleIndex = 6;
            this.colCode.Width = 76;
            // 
            // colBirthdate
            // 
            this.colBirthdate.Caption = "Дата рождения";
            this.colBirthdate.FieldName = "Birthdate";
            this.colBirthdate.Name = "colBirthdate";
            this.colBirthdate.Visible = true;
            this.colBirthdate.VisibleIndex = 7;
            this.colBirthdate.Width = 76;
            // 
            // colPhone
            // 
            this.colPhone.Caption = "Телефон";
            this.colPhone.FieldName = "Phone";
            this.colPhone.Name = "colPhone";
            this.colPhone.Visible = true;
            this.colPhone.VisibleIndex = 8;
            this.colPhone.Width = 77;
            // 
            // colDepartment
            // 
            this.colDepartment.Caption = "Подразделение";
            this.colDepartment.FieldName = "Department.Name";
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 9;
            // 
            // colWorkSchedule
            // 
            this.colWorkSchedule.Caption = "График работы";
            this.colWorkSchedule.FieldName = "WorkSchedule.Name";
            this.colWorkSchedule.Name = "colWorkSchedule";
            this.colWorkSchedule.Visible = true;
            this.colWorkSchedule.VisibleIndex = 9;
            // 
            // colAddress
            // 
            this.colAddress.Caption = "Адрес";
            this.colAddress.FieldName = "Address";
            this.colAddress.Name = "colAddress";
            // 
            // colPassportNumber
            // 
            this.colPassportNumber.Caption = "Номер паспорта";
            this.colPassportNumber.FieldName = "PassportNumber";
            this.colPassportNumber.Name = "colPassportNumber";
            // 
            // colPassportData
            // 
            this.colPassportData.Caption = "Паспортные данные";
            this.colPassportData.FieldName = "PassportData";
            this.colPassportData.Name = "colPassportData";
            // 
            // colRecruitmentOrder
            // 
            this.colRecruitmentOrder.Caption = "Номер приказа";
            this.colRecruitmentOrder.FieldName = "RecruitmentOrder";
            this.colRecruitmentOrder.Name = "colRecruitmentOrder";
            // 
            // colHiringDate
            // 
            this.colHiringDate.Caption = "Дата принятия на работу";
            this.colHiringDate.FieldName = "HiringDate";
            this.colHiringDate.Name = "colHiringDate";
            // 
            // employeGridControl
            // 
            this.employeGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.employeGridControl.DataSource = this.employeBindingSource;
            this.employeGridControl.Location = new System.Drawing.Point(-4, 91);
            this.employeGridControl.MainView = this.gridView1;
            this.employeGridControl.Name = "employeGridControl";
            this.employeGridControl.Size = new System.Drawing.Size(991, 543);
            this.employeGridControl.TabIndex = 1;
            this.employeGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.employeGridControl;
            this.gridView3.Name = "gridView3";
            // 
            // employePage
            // 
            this.employePage.Controls.Add(this.ribbonControl1);
            this.employePage.Controls.Add(this.employeGridControl);
            this.employePage.Location = new System.Drawing.Point(4, 22);
            this.employePage.Name = "employePage";
            this.employePage.Size = new System.Drawing.Size(983, 631);
            this.employePage.TabIndex = 0;
            this.employePage.Text = "Сотрудники";
            this.employePage.UseVisualStyleBackColor = true;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.addEmployeButton,
            this.editEmployeButton,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.printEmployeButton,
            this.exportEmployeButton,
            this.barButtonItem12,
            this.allFieldVisibleCheckbox,
            this.removeEmployeButton,
            this.barButtonItem14,
            this.importEmployeButton,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem9});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 23;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.employesPage});
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.ShowOnMultiplePages;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(983, 95);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // addEmployeButton
            // 
            this.addEmployeButton.Caption = "Новый сотрудник";
            this.addEmployeButton.Id = 2;
            this.addEmployeButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("addEmployeButton.ImageOptions.Image")));
            this.addEmployeButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("addEmployeButton.ImageOptions.LargeImage")));
            this.addEmployeButton.Name = "addEmployeButton";
            this.addEmployeButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.addEmployeButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addEmployeButton_ItemClick);
            // 
            // editEmployeButton
            // 
            this.editEmployeButton.Caption = "Редактировать";
            this.editEmployeButton.Id = 3;
            this.editEmployeButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("editEmployeButton.ImageOptions.Image")));
            this.editEmployeButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("editEmployeButton.ImageOptions.LargeImage")));
            this.editEmployeButton.Name = "editEmployeButton";
            this.editEmployeButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.editEmployeButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.editEmployeButton_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Новое подразделение";
            this.barButtonItem3.Id = 4;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.LargeImage")));
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Удалить";
            this.barButtonItem4.Id = 5;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.LargeImage")));
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Обнавить";
            this.barButtonItem5.Id = 6;
            this.barButtonItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.Image")));
            this.barButtonItem5.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.LargeImage")));
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Новая должность";
            this.barButtonItem6.Id = 7;
            this.barButtonItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.ImageOptions.Image")));
            this.barButtonItem6.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.ImageOptions.LargeImage")));
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Удалить";
            this.barButtonItem7.Id = 8;
            this.barButtonItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem7.ImageOptions.Image")));
            this.barButtonItem7.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem7.ImageOptions.LargeImage")));
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Обнавить";
            this.barButtonItem8.Id = 9;
            this.barButtonItem8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.ImageOptions.Image")));
            this.barButtonItem8.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.ImageOptions.LargeImage")));
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // printEmployeButton
            // 
            this.printEmployeButton.Caption = "Печать";
            this.printEmployeButton.Id = 10;
            this.printEmployeButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("printEmployeButton.ImageOptions.Image")));
            this.printEmployeButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("printEmployeButton.ImageOptions.LargeImage")));
            this.printEmployeButton.Name = "printEmployeButton";
            this.printEmployeButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.printEmployeButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.printEmployeButton_ItemClick);
            // 
            // exportEmployeButton
            // 
            this.exportEmployeButton.Caption = "Экспорт в Excel";
            this.exportEmployeButton.Id = 11;
            this.exportEmployeButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("exportEmployeButton.ImageOptions.Image")));
            this.exportEmployeButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("exportEmployeButton.ImageOptions.LargeImage")));
            this.exportEmployeButton.Name = "exportEmployeButton";
            this.exportEmployeButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.exportEmployeButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exportEmployeButton_ItemClick);
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Id = 18;
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // allFieldVisibleCheckbox
            // 
            this.allFieldVisibleCheckbox.Caption = "Все поля";
            this.allFieldVisibleCheckbox.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.allFieldVisibleCheckbox.Id = 15;
            this.allFieldVisibleCheckbox.Name = "allFieldVisibleCheckbox";
            this.allFieldVisibleCheckbox.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItem1_CheckedChanged_1);
            // 
            // removeEmployeButton
            // 
            this.removeEmployeButton.Caption = "Удалить";
            this.removeEmployeButton.Id = 16;
            this.removeEmployeButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("removeEmployeButton.ImageOptions.Image")));
            this.removeEmployeButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("removeEmployeButton.ImageOptions.LargeImage")));
            this.removeEmployeButton.Name = "removeEmployeButton";
            this.removeEmployeButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.removeEmployeButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.removeEmployeButton_ItemClick);
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Сохранить";
            this.barButtonItem14.Id = 17;
            this.barButtonItem14.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem14.ImageOptions.Image")));
            this.barButtonItem14.Name = "barButtonItem14";
            this.barButtonItem14.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // importEmployeButton
            // 
            this.importEmployeButton.Caption = "Импорт из Excel";
            this.importEmployeButton.Id = 19;
            this.importEmployeButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("importEmployeButton.ImageOptions.Image")));
            this.importEmployeButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("importEmployeButton.ImageOptions.LargeImage")));
            this.importEmployeButton.Name = "importEmployeButton";
            this.importEmployeButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.importEmployeButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.importEmployeButton_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Доступ и разрешение";
            this.barButtonItem1.Id = 20;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Синхронизация с оборудованием";
            this.barButtonItem2.Id = 21;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.LargeImage")));
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Шаблон для импорта";
            this.barButtonItem9.Id = 22;
            this.barButtonItem9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.ImageOptions.Image")));
            this.barButtonItem9.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.ImageOptions.LargeImage")));
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // employesPage
            // 
            this.employesPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup4,
            this.ribbonPageGroup5});
            this.employesPage.Name = "employesPage";
            this.employesPage.Text = "Сотрудники";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.addEmployeButton);
            this.ribbonPageGroup1.ItemLinks.Add(this.editEmployeButton);
            this.ribbonPageGroup1.ItemLinks.Add(this.removeEmployeButton);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Редактирование";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.importEmployeButton);
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem9);
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup4.ItemLinks.Add(this.printEmployeButton);
            this.ribbonPageGroup4.ItemLinks.Add(this.exportEmployeButton);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Печать и экспорт";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.allFieldVisibleCheckbox);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Отоброжение";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.employePage);
            this.tabControl1.Controls.Add(this.departmentPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(991, 657);
            this.tabControl1.TabIndex = 5;
            // 
            // DepartmentsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 657);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DepartmentsForm";
            this.Text = "Сотрудники";
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeBindingSource)).EndInit();
            this.departmentPage.ResumeLayout(false);
            this.departmentPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workScheduleLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.employePage.ResumeLayout(false);
            this.employePage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource departmentBindingSource;
        private System.Windows.Forms.BindingSource employeBindingSource;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.TabPage departmentPage;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl2;
        private DevExpress.XtraBars.BarButtonItem addDepartmentButton;
        private DevExpress.XtraBars.BarButtonItem editDepartmentButton;
        private DevExpress.XtraBars.BarButtonItem deleteDepartmentButton;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraGrid.GridControl departmentGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkSchedule1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit workScheduleLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colGender;
        private DevExpress.XtraGrid.Columns.GridColumn colFullname;
        private DevExpress.XtraGrid.Columns.GridColumn colPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colRank;
        private DevExpress.XtraGrid.Columns.GridColumn colTableNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBirthdate;
        private DevExpress.XtraGrid.Columns.GridColumn colPhone;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkSchedule;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colPassportNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPassportData;
        private DevExpress.XtraGrid.Columns.GridColumn colRecruitmentOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colHiringDate;
        private DevExpress.XtraGrid.GridControl employeGridControl;
        private System.Windows.Forms.TabPage employePage;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem addEmployeButton;
        private DevExpress.XtraBars.BarButtonItem editEmployeButton;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem printEmployeButton;
        private DevExpress.XtraBars.BarButtonItem exportEmployeButton;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarCheckItem allFieldVisibleCheckbox;
        private DevExpress.XtraBars.BarButtonItem removeEmployeButton;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem importEmployeButton;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Ribbon.RibbonPage employesPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.TabControl tabControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
    }
}