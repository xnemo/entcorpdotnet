﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using EntCorp.Data.Models;
using EntCorp.Helpers;
using EntCorp.Utils;

namespace EntCorp.Forms
{
    public partial class EmployeeCard : Form
    {
        public Employe _employe { get; set; }
        private int countPerPage = 20;
        private double pagesCount = 0;

        public EmployeeCard()
        {
            InitializeComponent();
        }
        

        private void EmployeeCard_Load(object sender, EventArgs e)
        {
            try
            {
                string bdate = "";

                if (!_employe.Birthdate.Equals(null))
                {
                    bdate = DateTime.Parse(_employe.Birthdate.ToString()).ToString("dd.MM.yyyy");
                }

                string hdate = "";

                if (!_employe.HiringDate.Equals(null))
                {
                    hdate = DateTime.Parse(_employe.HiringDate.ToString()).ToString("dd.MM.yyyy");
                }

                if (!String.IsNullOrEmpty(_employe.Photo) && File.Exists(_employe.Photo))
                {
                    photoField.Image = Image.FromFile(_employe.Photo);
                }
                _fullname.Text = _employe.Fullname;
                _department.Text = _employe.Department.Name;
                _position.Text = _employe.Position;
                _birthdate.Text = bdate;
                _phone.Text = _employe.Phone;
                _tableNum.Text = _employe.TableNumber;
                _orderNum.Text = _employe.RecruitmentOrder;
                _rank.Text = _employe.Rank.ToString();
                _orderDate.Text = hdate;

                
                pagesCount = (double)_employe.Visits.Count / countPerPage;
                pagesCount = Math.Ceiling(pagesCount);

                for (int i=1; i <= pagesCount; i++)
                {
                    pageCount.Items.Add(i.ToString());
                }
                pageCount.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert(ex.Message);
            }
        }

        private void LoadVisits(int page)
        {
            List<Visit> visits = _employe.Visits
                .OrderByDescending(v => v.RegisteredAt)
                .Skip((page - 1) * countPerPage)
                .Take(countPerPage)
                .ToList();

            gridControl1.DataSource = visits;
        }

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            ColumnView view = sender as ColumnView;

            if (e.Column.FieldName == "Event" &&
                e.ListSourceRowIndex != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                e.DisplayText = e.Value.ToString() == "IN" ? "Вход" : "Выход";
            }
        }

        private void печатьToolStripButton_Click(object sender, EventArgs e)
        {
            CompositeLink compLink = new CompositeLink(new PrintingSystem());
            PrintableComponentLink gridLink = new PrintableComponentLink();
            gridLink.Component = gridControl1;
            compLink.PaperKind = PaperKind.A4;
            compLink.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            compLink.Margins.Right = 5;
            compLink.Margins.Left = 5;
            compLink.Margins.Top = 5;
            compLink.Margins.Bottom = 5;
            Link headerLink = new Link();
            headerLink.CreateDetailArea += new CreateAreaEventHandler(headerLink_CreateDetailArea);
            Link footerLink = new Link();
            footerLink.CreateDetailArea += new CreateAreaEventHandler(footerLink_CreateDetailArea);
            compLink.Links.Add(headerLink);
            compLink.Links.Add(gridLink);
            compLink.Links.Add(footerLink);
            compLink.ShowPreviewDialog();
        }

        void footerLink_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            
        }

        void headerLink_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Center);
            e.Graph.Font = new Font("Tahoma", 12, FontStyle.Bold);
            RectangleF rec = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 25);
            e.Graph.DrawString($"{_employe.Fullname} [{_employe.Department.Name} - {_employe.Position}]", Color.Black, rec, BorderSide.None);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            LoadPicture(sender, e);
        }

        private void LoadPicture(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            string directory = "photos";
            string photo = "noavatar.png";
            string picFilePath = String.Format("{0}\\{1}", directory, photo);

            GridView currentView = sender as GridView;
            Rectangle r = e.Bounds;

            int dx = 4;
            r.X += e.Bounds.Height + dx * 3;
            r.Width -= (e.Bounds.Height + dx * 4);

            if (e.Column.FieldName == "Photo")
            {
                e.DisplayText = string.Empty;

                if (currentView.GetRowCellValue(e.RowHandle, currentView.Columns["Photo"]) != null)
                {
                    photo = currentView.GetRowCellValue(e.RowHandle, currentView.Columns["Photo"]).ToString();
                }

                if (File.Exists(photo))
                {
                    picFilePath = photo;
                }

                Image img = Image.FromFile(picFilePath);
                e.Graphics.DrawImage(img, e.Bounds.X + dx, e.Bounds.Y, e.Bounds.Height, e.Bounds.Height);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            gridView1.Columns["Photo"].Visible = !gridView1.Columns["Photo"].Visible;
            gridView1.RowHeight = gridView1.Columns["Photo"].Visible ? 75 : -1;
        }

        private void pageCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadVisits(Convert.ToInt32(pageCount.Text));
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (pageCount.SelectedIndex != 0)
                pageCount.SelectedIndex--;
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (pageCount.SelectedIndex < pagesCount - 1)
                pageCount.SelectedIndex++;
        }
    }
}