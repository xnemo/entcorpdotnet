﻿namespace EntCorp.Forms
{
    partial class DeviceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label readerTypeLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeviceForm));
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.nameField = new System.Windows.Forms.TextBox();
            this.bsDevice = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.ipLabel = new System.Windows.Forms.Label();
            this.activeField = new System.Windows.Forms.CheckBox();
            this.IPfield = new System.Windows.Forms.TextBox();
            this.readerTypeComboBox = new System.Windows.Forms.ComboBox();
            this.gateLabel = new System.Windows.Forms.Label();
            this.gateNumberField = new System.Windows.Forms.NumericUpDown();
            readerTypeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bsDevice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gateNumberField)).BeginInit();
            this.SuspendLayout();
            // 
            // readerTypeLabel
            // 
            readerTypeLabel.AutoSize = true;
            readerTypeLabel.Location = new System.Drawing.Point(12, 43);
            readerTypeLabel.Name = "readerTypeLabel";
            readerTypeLabel.Size = new System.Drawing.Size(68, 13);
            readerTypeLabel.TabIndex = 22;
            readerTypeLabel.Text = "Тип ридера:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.simpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(388, 98);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(95, 31);
            this.simpleButton1.TabIndex = 19;
            this.simpleButton1.Text = "Сохранить";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // nameField
            // 
            this.nameField.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsDevice, "Name", true));
            this.nameField.Location = new System.Drawing.Point(85, 13);
            this.nameField.Name = "nameField";
            this.nameField.Size = new System.Drawing.Size(400, 20);
            this.nameField.TabIndex = 1;
            // 
            // bsDevice
            // 
            this.bsDevice.DataSource = typeof(EntCorp.Data.Models.Device);
            this.bsDevice.CurrentChanged += new System.EventHandler(this.bsDevice_CurrentChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Название:";
            // 
            // ipLabel
            // 
            this.ipLabel.AutoSize = true;
            this.ipLabel.Location = new System.Drawing.Point(12, 70);
            this.ipLabel.Name = "ipLabel";
            this.ipLabel.Size = new System.Drawing.Size(53, 13);
            this.ipLabel.TabIndex = 22;
            this.ipLabel.Text = "IP-адрес:";
            // 
            // activeField
            // 
            this.activeField.AutoSize = true;
            this.activeField.Checked = true;
            this.activeField.CheckState = System.Windows.Forms.CheckState.Checked;
            this.activeField.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.bsDevice, "Active", true));
            this.activeField.Location = new System.Drawing.Point(85, 92);
            this.activeField.Name = "activeField";
            this.activeField.Size = new System.Drawing.Size(86, 17);
            this.activeField.TabIndex = 3;
            this.activeField.Text = "Вести учет?";
            this.activeField.UseVisualStyleBackColor = true;
            // 
            // IPfield
            // 
            this.IPfield.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsDevice, "IpAdress", true));
            this.IPfield.Location = new System.Drawing.Point(85, 66);
            this.IPfield.Name = "IPfield";
            this.IPfield.Size = new System.Drawing.Size(159, 20);
            this.IPfield.TabIndex = 2;
            // 
            // readerTypeComboBox
            // 
            this.readerTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsDevice, "ReaderType", true));
            this.readerTypeComboBox.FormattingEnabled = true;
            this.readerTypeComboBox.Location = new System.Drawing.Point(85, 39);
            this.readerTypeComboBox.Name = "readerTypeComboBox";
            this.readerTypeComboBox.Size = new System.Drawing.Size(159, 21);
            this.readerTypeComboBox.TabIndex = 23;
            this.readerTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.readerTypeComboBox_SelectedIndexChanged);
            // 
            // gateLabel
            // 
            this.gateLabel.AutoSize = true;
            this.gateLabel.Location = new System.Drawing.Point(260, 70);
            this.gateLabel.Name = "gateLabel";
            this.gateLabel.Size = new System.Drawing.Size(88, 13);
            this.gateLabel.TabIndex = 26;
            this.gateLabel.Text = "Номер прохода:";
            // 
            // gateNumberField
            // 
            this.gateNumberField.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bsDevice, "Gate", true));
            this.gateNumberField.Location = new System.Drawing.Point(354, 66);
            this.gateNumberField.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.gateNumberField.Name = "gateNumberField";
            this.gateNumberField.Size = new System.Drawing.Size(129, 20);
            this.gateNumberField.TabIndex = 27;
            // 
            // DeviceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 141);
            this.Controls.Add(this.gateNumberField);
            this.Controls.Add(this.gateLabel);
            this.Controls.Add(readerTypeLabel);
            this.Controls.Add(this.readerTypeComboBox);
            this.Controls.Add(this.IPfield);
            this.Controls.Add(this.activeField);
            this.Controls.Add(this.ipLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nameField);
            this.Controls.Add(this.simpleButton1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DeviceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оборудование";
            ((System.ComponentModel.ISupportInitialize)(this.bsDevice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gateNumberField)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.TextBox nameField;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ipLabel;
        private System.Windows.Forms.CheckBox activeField;
        private System.Windows.Forms.TextBox IPfield;
        private System.Windows.Forms.BindingSource bsDevice;
        private System.Windows.Forms.ComboBox readerTypeComboBox;
        private System.Windows.Forms.Label gateLabel;
        private System.Windows.Forms.NumericUpDown gateNumberField;
    }
}