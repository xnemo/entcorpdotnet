﻿using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using EntCorp.Contracts;
using EntCorp.Data.Models;
using EntCorp.Service;
using EntCorp.Utils;

namespace EntCorp.Forms
{
    public partial class TabelForm : Form
    {
        DataTable dt;
        private DepartmentService _departmentService;
        private EmployeService _employeService;
        private VisitService _visitService;
        public TabelForm(IRepositoryWrapper repoWrapper)
        {
            InitializeComponent();
            dt = new DataTable();
            _departmentService = new DepartmentService(repoWrapper);
            _employeService = new EmployeService(repoWrapper);
            _visitService = new VisitService(repoWrapper);
        }

        private void TabelForm_Load(object sender, EventArgs e)
        {
            dateFilterBarItem.EditValue = DateTime.Now;
            departmentsFilter.DataSource = _departmentService.GetAllDepartments();
            departmentsFilter.DisplayMember = "Name";
            departmentsFilter.ValueMember = "Id";
            departmentsFilter.AllowNullInput = DefaultBoolean.True;
        }

        private void InitailizeGrid(DateTime eDate)
        {
            int daysCount = DateTime.DaysInMonth(eDate.Year, eDate.Month);

            gridView1.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            gridView1.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            gridView1.OptionsPrint.UsePrintStyles = true;

            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left,
                Caption = "Таб. номер",
                FieldName = "TableNumber",
                VisibleIndex = 0,
                Width = 60
            });
            dt.Columns.Add("TableNumber", typeof(string));

            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left,
                Caption = "Фамилия, Имя, Отчество",
                FieldName = "Fullname",
                VisibleIndex = 1,
                Width = 170
            });
            dt.Columns.Add("Fullname", typeof(string));

            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left,
                Caption = "Должность",
                FieldName = "Position",
                VisibleIndex = 2,
                Width = 150
            });
            dt.Columns.Add("Position", typeof(string));

            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left,
                Caption = "Разряд",
                FieldName = "Rank",
                VisibleIndex = 3,
                Width = 65
            });
            dt.Columns.Add("Rank", typeof(string));

            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left,
                Caption = "Подразделение",
                FieldName = "Department",
                VisibleIndex = 4,
                Width = 150
            });
            dt.Columns.Add("Department", typeof(string));

            for (int i = 1; i <= daysCount; i++)
            {
                DevExpress.XtraGrid.Columns.GridColumn newColumn = new DevExpress.XtraGrid.Columns.GridColumn();
                newColumn.Caption = i.ToString();
                newColumn.FieldName = String.Format("M{0}", i);
                newColumn.VisibleIndex = i+5;
                newColumn.Width = 30;
                newColumn.AppearanceCell.Options.UseBackColor = true;

                if(Convert.ToDateTime($"{i}.{eDate.Month}.{eDate.Year}").DayOfWeek==DayOfWeek.Sunday)
                    newColumn.AppearanceCell.BackColor = Color.LightGray;

                gridView1.Columns.Add(newColumn);
                dt.Columns.Add(String.Format("M{0}", i));
            }

            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right,
                Caption = "Кол-во рабочих дней",
                FieldName = "WorkDaysCount",
                VisibleIndex = 41,
                Width = 65
            });
            dt.Columns.Add("WorkDaysCount", typeof(int));

            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right,
                Caption = "Кол-во отраб. дней",
                FieldName = "WorkedDaysCount",
                VisibleIndex = 42,
                Width = 60
            });
            dt.Columns.Add("WorkedDaysCount", typeof(int));

            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            {
                Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right,
                Caption = "Кол-во отраб. часов",
                FieldName = "WorkedHoursCount",
                VisibleIndex = 43,
                Width = 60
            });
            dt.Columns.Add("WorkedHoursCount");

            gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            gridView1.ColumnPanelRowHeight = 50;
            gridView1.OptionsView.ColumnAutoWidth = false;

            gridControl1.DataSource = dt;
        }

        private void LoadTable(DateTime eDate)
        {
            int daysCount = DateTime.DaysInMonth(eDate.Year, eDate.Month);
            var employes = _employeService.GetAllEmployesWithDepartments();
            if (departmentFilterBarItem.EditValue != null)
            {
                int departmentId = (int)departmentFilterBarItem.EditValue;
                employes = _employeService.GetEmployeByDepartment(departmentId);
            }

            foreach (var em in employes)
            {
                DataRow newRow = dt.NewRow();
                newRow["Fullname"] = em.Fullname;
                newRow["TableNumber"] = em.TableNumber;
                newRow["Department"] = em.Department;
                newRow["Position"] = em.Position;
                newRow["Rank"] = em.Rank;

                int workedDays = 0;
                double workedHours = 0;

                for (int d = 1; d <= daysCount; d++)
                {
                    DateTime currentDate = DateTime.Parse($"{d}.{eDate.Month}.{eDate.Year}");
                    double i_time = 0, hours = 0, timespan = 0;
                    bool in_b = false;

                    Visit.Events lastEvent = Visit.Events.OUT;

                    var visitList = _visitService.GetVisitsByPersonAndDate(em.Id, currentDate);

                    foreach (var v in visitList)
                    {
                        if (v.Event.Equals(Visit.Events.IN) && lastEvent.Equals(Visit.Events.OUT))
                        {
                            i_time = TimeHelper.ConvertToUnixTimestamp(v.RegisteredAt);
                            in_b = true;
                        }

                        if (v.Event.Equals(Visit.Events.OUT) && in_b)
                        {
                            timespan = timespan + (TimeHelper.ConvertToUnixTimestamp(v.RegisteredAt) - i_time);
                            in_b = false;
                            i_time = 0;
                        }
                        lastEvent = v.Event;
                    }

                    double minutes = timespan % 60;
                    hours = Math.Round(Math.Round(timespan / 60) / 60, 2);

                    newRow[String.Format("M{0}", d)] = String.Format("{0}:{1}",(int)hours, minutes.ToString().PadLeft(2, '0'));
                    newRow["WorkDaysCount"] = daysCount;

                    workedHours = workedHours + timespan;

                    if (hours > 0)
                    {
                        workedDays++;
                    }
                }

                newRow["WorkedDaysCount"] = workedDays;

                newRow["WorkedHoursCount"] = String.Format("{0}:{1}", (int)Math.Round(Math.Round(workedHours / 60) / 60, 2), (workedHours % 60).ToString().PadLeft(2, '0'));
                dt.Rows.Add(newRow);
            }
        }

        private void dateFilterBarItem_EditValueChanged(object sender, EventArgs e)
        {
            this.LoadData();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CompositeLink compLink = this.getCompositeLink();
            compLink.ShowPreviewDialog();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string fileName = @"EmployeTable.xls";
            CompositeLink compLink = this.getCompositeLink();
            compLink.ExportToXls(fileName);
            Process.Start(fileName);
        }

        private CompositeLink getCompositeLink()
        {
            CompositeLink compLink = new CompositeLink(new PrintingSystem());
            PrintableComponentLink gridLink = new PrintableComponentLink();
            gridLink.Component = gridControl1;
            compLink.Landscape = true;
            compLink.PaperKind = PaperKind.A4;
            compLink.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            compLink.Margins.Right = 5;
            compLink.Margins.Left = 5;
            compLink.Margins.Top = 5;
            compLink.Margins.Bottom = 5;
            Link headerLink = new Link();
            headerLink.CreateDetailArea += new CreateAreaEventHandler(headerLink_CreateDetailArea);
            compLink.Links.Add(headerLink);
            compLink.Links.Add(gridLink);

            return compLink;
        }


        void headerLink_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            string title = DateTime.Parse(dateFilterBarItem.EditValue.ToString()).ToString("MMMM yyyy");

            e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Center);
            e.Graph.Font = new Font("Tahoma", 12, FontStyle.Bold);
            RectangleF rec = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 25);
            e.Graph.DrawString($"Табель за {title}", Color.Black, rec, BorderSide.None);
        }

        private void barEditItem5_EditValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            dt.Columns.Clear();
            dt.Rows.Clear();

            gridView1.Columns.Clear();
            gridView1.Columns.Clear();

            gridView1.RefreshData();
            gridControl1.Refresh();

            DateTime filterDate = Convert.ToDateTime(dateFilterBarItem.EditValue);
            InitailizeGrid(filterDate);
            LoadTable(filterDate);

            gridView1.Columns.ColumnByFieldName("Department").GroupIndex = 0;
        }
    }
}
