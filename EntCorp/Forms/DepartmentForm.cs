﻿using EntCorp.Data.Models;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using EntCorp.Utils;

namespace EntCorp.Forms
{
    public partial class DepartmentForm : Form
    {
        public Department Department { get; set; }
        private IEnumerable<WorkSchedule> _workSchedules;

        public DepartmentForm(IEnumerable<WorkSchedule> workSchedules)
        {
            InitializeComponent();
            _workSchedules = workSchedules;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ActiveControl = nameField;
            departmentBindingSource.DataSource = Department;
            workScheduleBindingSource.DataSource = _workSchedules;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //SaveDepartrment();
            DialogResult = DialogResult.OK;
        }

        private void SaveDepartrment()
        {
            try
            {
                //if (Department.Id == 0)
                //{
                //    _departmentService.UpdateDepartment(Department);
                //}
                //else
                //{
                //    _departmentService.CreateDepartment(Department);
                //}
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }
    }
}
