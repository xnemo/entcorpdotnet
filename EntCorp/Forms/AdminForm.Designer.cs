﻿namespace EntCorp.Forms
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.devicePage = new System.Windows.Forms.TabPage();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.deviceGridControl = new DevExpress.XtraGrid.GridControl();
            this.deviceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastPing = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedAt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIpAdress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReaderType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGateNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colToken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.usersPage = new System.Windows.Forms.TabPage();
            this.userGridControl = new DevExpress.XtraGrid.GridControl();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colUsername = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastLogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribbonControl2 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.addUserButton = new DevExpress.XtraBars.BarButtonItem();
            this.editUserButton = new DevExpress.XtraBars.BarButtonItem();
            this.deleteUserButton = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.settingsPage = new System.Windows.Forms.TabPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.tabControl1.SuspendLayout();
            this.devicePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.usersPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.devicePage);
            this.tabControl1.Controls.Add(this.usersPage);
            this.tabControl1.Controls.Add(this.settingsPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1147, 576);
            this.tabControl1.TabIndex = 0;
            // 
            // devicePage
            // 
            this.devicePage.Controls.Add(this.ribbonControl1);
            this.devicePage.Controls.Add(this.deviceGridControl);
            this.devicePage.Location = new System.Drawing.Point(4, 22);
            this.devicePage.Name = "devicePage";
            this.devicePage.Size = new System.Drawing.Size(1139, 550);
            this.devicePage.TabIndex = 0;
            this.devicePage.Text = "Оборудование";
            this.devicePage.UseVisualStyleBackColor = true;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 4;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowPageHeadersInFormCaption = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbonControl1.Size = new System.Drawing.Size(1139, 95);
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Добавить";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Редактировать";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Удалить";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Редактирование";
            // 
            // deviceGridControl
            // 
            this.deviceGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceGridControl.DataSource = this.deviceBindingSource;
            this.deviceGridControl.Location = new System.Drawing.Point(0, 93);
            this.deviceGridControl.MainView = this.gridView1;
            this.deviceGridControl.Name = "deviceGridControl";
            this.deviceGridControl.Size = new System.Drawing.Size(1139, 460);
            this.deviceGridControl.TabIndex = 0;
            this.deviceGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // deviceBindingSource
            // 
            this.deviceBindingSource.DataSource = typeof(EntCorp.Data.Models.Device);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colName,
            this.colActive,
            this.colLastPing,
            this.colCreatedAt,
            this.colIpAdress,
            this.colReaderType,
            this.colGateNumber,
            this.colToken});
            this.gridView1.GridControl = this.deviceGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.Visible = true;
            this.colId.VisibleIndex = 0;
            this.colId.Width = 22;
            // 
            // colName
            // 
            this.colName.Caption = "Название оборудования";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            this.colName.Width = 340;
            // 
            // colActive
            // 
            this.colActive.Caption = "Вести учет?";
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 2;
            this.colActive.Width = 87;
            // 
            // colLastPing
            // 
            this.colLastPing.Caption = "Последнее подключение";
            this.colLastPing.FieldName = "LastPing";
            this.colLastPing.Name = "colLastPing";
            this.colLastPing.Width = 128;
            // 
            // colCreatedAt
            // 
            this.colCreatedAt.Caption = "Дата добавления";
            this.colCreatedAt.FieldName = "CreatedAt";
            this.colCreatedAt.Name = "colCreatedAt";
            this.colCreatedAt.Visible = true;
            this.colCreatedAt.VisibleIndex = 7;
            this.colCreatedAt.Width = 164;
            // 
            // colIpAdress
            // 
            this.colIpAdress.Caption = "IpAdress/COM порт";
            this.colIpAdress.FieldName = "IpAdress";
            this.colIpAdress.Name = "colIpAdress";
            this.colIpAdress.Visible = true;
            this.colIpAdress.VisibleIndex = 3;
            this.colIpAdress.Width = 123;
            // 
            // colReaderType
            // 
            this.colReaderType.Caption = "Тип ридера";
            this.colReaderType.FieldName = "ReaderType";
            this.colReaderType.Name = "colReaderType";
            this.colReaderType.Visible = true;
            this.colReaderType.VisibleIndex = 4;
            this.colReaderType.Width = 93;
            // 
            // colGateNumber
            // 
            this.colGateNumber.Caption = "Номер прохода";
            this.colGateNumber.FieldName = "Gate";
            this.colGateNumber.Name = "colGateNumber";
            this.colGateNumber.Visible = true;
            this.colGateNumber.VisibleIndex = 5;
            // 
            // colToken
            // 
            this.colToken.Caption = "Токен";
            this.colToken.FieldName = "Token";
            this.colToken.Name = "colToken";
            this.colToken.OptionsColumn.ReadOnly = true;
            this.colToken.Visible = true;
            this.colToken.VisibleIndex = 6;
            this.colToken.Width = 164;
            // 
            // usersPage
            // 
            this.usersPage.Controls.Add(this.userGridControl);
            this.usersPage.Controls.Add(this.ribbonControl2);
            this.usersPage.Location = new System.Drawing.Point(4, 22);
            this.usersPage.Name = "usersPage";
            this.usersPage.Size = new System.Drawing.Size(1139, 550);
            this.usersPage.TabIndex = 1;
            this.usersPage.Text = "Пользователи";
            this.usersPage.UseVisualStyleBackColor = true;
            // 
            // userGridControl
            // 
            this.userGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userGridControl.DataSource = this.userBindingSource;
            this.userGridControl.Location = new System.Drawing.Point(0, 93);
            this.userGridControl.MainView = this.gridView2;
            this.userGridControl.MenuManager = this.ribbonControl1;
            this.userGridControl.Name = "userGridControl";
            this.userGridControl.Size = new System.Drawing.Size(1139, 461);
            this.userGridControl.TabIndex = 1;
            this.userGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(EntCorp.Data.Models.User);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUsername,
            this.colLastLogin,
            this.colRole,
            this.colId1});
            this.gridView2.GridControl = this.userGridControl;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // colUsername
            // 
            this.colUsername.Caption = "Логин пользователя";
            this.colUsername.FieldName = "Username";
            this.colUsername.Name = "colUsername";
            this.colUsername.Visible = true;
            this.colUsername.VisibleIndex = 1;
            this.colUsername.Width = 368;
            // 
            // colLastLogin
            // 
            this.colLastLogin.Caption = "Последний вход";
            this.colLastLogin.FieldName = "LastLogin";
            this.colLastLogin.Name = "colLastLogin";
            this.colLastLogin.Visible = true;
            this.colLastLogin.VisibleIndex = 2;
            this.colLastLogin.Width = 166;
            // 
            // colRole
            // 
            this.colRole.Caption = "Роль";
            this.colRole.FieldName = "Role";
            this.colRole.Name = "colRole";
            this.colRole.Visible = true;
            this.colRole.VisibleIndex = 3;
            this.colRole.Width = 261;
            // 
            // colId1
            // 
            this.colId1.FieldName = "Id";
            this.colId1.Name = "colId1";
            this.colId1.Visible = true;
            this.colId1.VisibleIndex = 0;
            this.colId1.Width = 32;
            // 
            // ribbonControl2
            // 
            this.ribbonControl2.ExpandCollapseItem.Id = 0;
            this.ribbonControl2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl2.ExpandCollapseItem,
            this.addUserButton,
            this.editUserButton,
            this.deleteUserButton});
            this.ribbonControl2.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl2.MaxItemId = 4;
            this.ribbonControl2.Name = "ribbonControl2";
            this.ribbonControl2.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage2});
            this.ribbonControl2.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl2.ShowPageHeadersInFormCaption = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl2.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbonControl2.ShowToolbarCustomizeItem = false;
            this.ribbonControl2.Size = new System.Drawing.Size(1139, 95);
            this.ribbonControl2.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl2.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // addUserButton
            // 
            this.addUserButton.Caption = "Добавить";
            this.addUserButton.Id = 1;
            this.addUserButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("addUserButton.ImageOptions.Image")));
            this.addUserButton.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("addUserButton.ImageOptions.LargeImage")));
            this.addUserButton.Name = "addUserButton";
            this.addUserButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.addUserButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addUserButton_ItemClick);
            // 
            // editUserButton
            // 
            this.editUserButton.Caption = "Редактировать";
            this.editUserButton.Id = 2;
            this.editUserButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("editUserButton.ImageOptions.Image")));
            this.editUserButton.Name = "editUserButton";
            this.editUserButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.editUserButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.editUserButton_ItemClick);
            // 
            // deleteUserButton
            // 
            this.deleteUserButton.Caption = "Удалить";
            this.deleteUserButton.Id = 3;
            this.deleteUserButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("deleteUserButton.ImageOptions.Image")));
            this.deleteUserButton.Name = "deleteUserButton";
            this.deleteUserButton.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.deleteUserButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteUserButton_ItemClick);
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "ribbonPage2";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.addUserButton);
            this.ribbonPageGroup2.ItemLinks.Add(this.editUserButton);
            this.ribbonPageGroup2.ItemLinks.Add(this.deleteUserButton);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Редактирование";
            // 
            // settingsPage
            // 
            this.settingsPage.Location = new System.Drawing.Point(4, 22);
            this.settingsPage.Name = "settingsPage";
            this.settingsPage.Padding = new System.Windows.Forms.Padding(3);
            this.settingsPage.Size = new System.Drawing.Size(1139, 550);
            this.settingsPage.TabIndex = 2;
            this.settingsPage.Text = "Настройки";
            this.settingsPage.UseVisualStyleBackColor = true;
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Редактирование";
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 576);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdminForm";
            this.Text = "Администрирование";
            this.Load += new System.EventHandler(this.AdminForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.devicePage.ResumeLayout(false);
            this.devicePage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.usersPage.ResumeLayout(false);
            this.usersPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage devicePage;
        private System.Windows.Forms.TabPage usersPage;
        private DevExpress.XtraGrid.GridControl deviceGridControl;
        private System.Windows.Forms.BindingSource deviceBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colToken;
        private DevExpress.XtraGrid.Columns.GridColumn colLastPing;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedAt;
        private DevExpress.XtraGrid.Columns.GridColumn colIpAdress;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl2;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraGrid.GridControl userGridControl;
        private System.Windows.Forms.BindingSource userBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colUsername;
        private DevExpress.XtraGrid.Columns.GridColumn colLastLogin;
        private DevExpress.XtraGrid.Columns.GridColumn colRole;
        private DevExpress.XtraGrid.Columns.GridColumn colId1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem addUserButton;
        private DevExpress.XtraBars.BarButtonItem editUserButton;
        private DevExpress.XtraBars.BarButtonItem deleteUserButton;
        private DevExpress.XtraGrid.Columns.GridColumn colReaderType;
        private System.Windows.Forms.TabPage settingsPage;
        private DevExpress.XtraGrid.Columns.GridColumn colGateNumber;
    }
}