﻿using System;
using System.Windows.Forms;
using EntCorp.Contracts;
using EntCorp.Data.Models;
using EntCorp.Service;
using EntCorp.Utils;

namespace EntCorp.Forms
{
    public partial class UserForm : Form
    {
        public User User { get; set; }
        private UserService _userService;

        public UserForm()
        {
            InitializeComponent();
        }

        public UserForm(UserService userService)
        {
            InitializeComponent();
            _userService = userService; 
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            roleComboBox.DataSource = Enum.GetValues(typeof(Roles));
            roleComboBox.DisplayMember = "Value";
            roleComboBox.DataBindings.Add("SelectedValue", userBs, "Role", true, DataSourceUpdateMode.OnPropertyChanged);

            ActiveControl = usernameTextEdit;
            userBs.DataSource = User;

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            SaveUser();
            DialogResult = DialogResult.OK;
        }

        private void SaveUser()
        {
            try
            {
                if (User.Id == 0)
                {
                    _userService.CreateUser(User);
                }
                else
                {
                    _userService.UpdateUser(User);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }
    }
}
