﻿using EntCorp.Data.Models;
using EntCorp.Helpers;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using EntCorp.Utils;
using DevExpress.XtraEditors.DXErrorProvider;
using EntCorp.Service;
using System.Linq;

namespace EntCorp.Forms
{
    public partial class EmployeForm : Form
    {
        public Employe Employe { get; set; }
        DXErrorProvider errorProvider = new DXErrorProvider();
        AdmissionService _admissionService;
        IEnumerable<Admission> _admissions;

        public EmployeForm()
        {
            InitializeComponent();
        }

        public EmployeForm(IEnumerable<Department> departments, 
            IEnumerable<WorkSchedule> workSchedules, IEnumerable<Device> devices, AdmissionService admissionService)
        {
            InitializeComponent();
            workScheduleBindingSource.DataSource = workSchedules;
            departmentsBindingSource.DataSource = departments;
            deviceBindingSource.DataSource = devices;
            _admissionService = admissionService;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ActiveControl = fullnameField;
            employeBindingSource.DataSource = Employe;
            codeDeviceText.Text = Employe.Code;
            genderField.Text = Employe.Gender == true ? "М" : "Ж";
            LoadAdmissions();
        }

        private void LoadAdmissions()
        {
            _admissions = _admissionService.GetAdmissionsByEmploye(Employe.Id);
            admissionBindingSource.DataSource = _admissions;
        }

        private void EmployeForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(Employe.Photo) && File.Exists(Employe.Photo))
                {
                    photoField.Image = Image.FromFile(Employe.Photo);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert(ex.Message);
            }
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            try
            {
                if ((int)departmentLookUpEdit.EditValue == 0)
                {
                    errorProvider.SetError(departmentLookUpEdit, "Выберите подразделение", ErrorType.Critical);
                    return;
                }

                if (photoField.Image != null)
                {
                    Employe.Photo = FileUpload.SavePhoto(photoField.Image, Employe.DepartmentId.ToString());
                }

                Employe.Gender = genderField.Text == "М" ? true : false;
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string token = codeDeviceText.Text;
                Device device = (Device)devicesCombo.SelectedItem;

                if(!_admissions.Any(a => a.Device == device && a.EmployeId == Employe.Id && a.Token == token && a.Deleted != true))
                {
                    Admission admission = new Admission();
                    admission.Device = device;
                    admission.Token = token;
                    admission.EmployeId = Employe.Id;

                    _admissionService.CreateAdmission(admission);

                    LoadAdmissions();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert(ex.Message);
            }
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (admissionsGridView.CurrentRow != null)
                {
                    var admission = (Admission)admissionsGridView.SelectedRows[0].DataBoundItem;
                    //Employe.Admissions.Remove(admission);
                    _admissionService.RemoveAdmission(admission);
                    admissionsGridView.Rows.Remove(admissionsGridView.CurrentRow);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
                Alerts.SetAlert(ex.Message);
            }
        }
    }
}
