﻿using EntCorp.Data.Models;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EntCorp.Forms
{
    public partial class SetAdmissionForm : Form
    {
        public string DeviceId => comboBox1.SelectedValue.ToString();
        public IEnumerable<Device> Devices { get; set; }
        public SetAdmissionForm()
        {
            InitializeComponent();
        }

        private void SetAdmissionForm_Load(object sender, EventArgs e)
        {
            deviceBindingSource.DataSource = Devices;
        }

    }
}
