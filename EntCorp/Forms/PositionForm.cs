﻿using System;
using System.Windows.Forms;
using EntCorp.Data.Models;

namespace EntCorp.Forms
{
    public partial class PositionForm : Form
    {
        public Position Position { get; set; }
        public PositionForm()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ActiveControl = nameField;
            positionBindingSource.DataSource = Position;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
