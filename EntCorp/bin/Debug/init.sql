﻿CREATE TABLE `devices` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` longtext,
  `Active` tinyint(1) NOT NULL,
  `Token` longtext,
  `LastPing` datetime DEFAULT NULL,
  `IpAdress` longtext,
  `ReaderType` int(11) NOT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `workschedules` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` longtext,
  `Type` int(11) NOT NULL,
  `Default` tinyint(1) NOT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `workschemas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `DayNumber` int(11) DEFAULT NULL,
  `StartTime` longtext,
  `EndTime` longtext,
  `Hours` float NOT NULL,
  `WorkScheduleId` int(11) NOT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_WorkScheduleId` (`WorkScheduleId` DESC) USING BTREE,
  KEY `FK_WorkSchemas_WorkSchedules_WorkScheduleId` (`WorkScheduleId`),
  CONSTRAINT `FK_WorkSchemas_WorkSchedules_WorkScheduleId` FOREIGN KEY (`WorkScheduleId`) REFERENCES `workschedules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


INSERT INTO `workschedules` (`Id`,`Name`,`Type`,`Default`,`CreatedAt`,`UpdatedAt`,`Deleted`) VALUES (1,'Стандартный',1,1,'2019-01-10 13:29:59','2019-01-10 13:29:59',NULL);

INSERT INTO `workschemas` (`Id`,`DayNumber`,`StartTime`,`EndTime`,`Hours`,`WorkScheduleId`,`CreatedAt`,`UpdatedAt`,`Deleted`) VALUES (1,1,'9:00','18:00',1,1,'2019-01-10 13:32:02','2019-01-10 13:32:02',NULL);
INSERT INTO `workschemas` (`Id`,`DayNumber`,`StartTime`,`EndTime`,`Hours`,`WorkScheduleId`,`CreatedAt`,`UpdatedAt`,`Deleted`) VALUES (2,2,'9:00','18:00',1,1,'2019-01-10 13:32:02','2019-01-10 13:32:02',NULL);
INSERT INTO `workschemas` (`Id`,`DayNumber`,`StartTime`,`EndTime`,`Hours`,`WorkScheduleId`,`CreatedAt`,`UpdatedAt`,`Deleted`) VALUES (3,3,'9:00','18:00',1,1,'2019-01-10 13:32:02','2019-01-10 13:32:02',NULL);
INSERT INTO `workschemas` (`Id`,`DayNumber`,`StartTime`,`EndTime`,`Hours`,`WorkScheduleId`,`CreatedAt`,`UpdatedAt`,`Deleted`) VALUES (4,4,'9:00','18:00',1,1,'2019-01-10 13:32:02','2019-01-10 13:32:02',NULL);
INSERT INTO `workschemas` (`Id`,`DayNumber`,`StartTime`,`EndTime`,`Hours`,`WorkScheduleId`,`CreatedAt`,`UpdatedAt`,`Deleted`) VALUES (5,5,'9:00','18:00',1,1,'2019-01-10 13:32:02','2019-01-10 13:32:02',NULL);
INSERT INTO `workschemas` (`Id`,`DayNumber`,`StartTime`,`EndTime`,`Hours`,`WorkScheduleId`,`CreatedAt`,`UpdatedAt`,`Deleted`) VALUES (6,6,'9:00','18:00',1,1,'2019-01-10 13:32:02','2019-01-10 13:32:02',NULL);

CREATE TABLE `departments` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` longtext,
  `Active` tinyint(1) NOT NULL,
  `WorkScheduleId` int(11) NOT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_WorkScheduleId` (`WorkScheduleId` DESC) USING BTREE,
  KEY `FK_Departments_WorkSchedules_WorkScheduleId` (`WorkScheduleId`),
  CONSTRAINT `FK_Departments_WorkSchedules_WorkScheduleId` FOREIGN KEY (`WorkScheduleId`) REFERENCES `workschedules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8;

CREATE TABLE `positions` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` longtext,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

CREATE TABLE `employes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Fullname` longtext,
  `Birthdate` datetime DEFAULT NULL,
  `Code` longtext,
  `Active` tinyint(1) NOT NULL,
  `Gender` tinyint(1) NOT NULL,
  `Photo` longtext,
  `TableNumber` longtext,
  `Rank` int(11) NOT NULL,
  `PassportNumber` longtext,
  `PassportData` longtext,
  `Address` longtext,
  `Phone` longtext,
  `DepartmentId` int(11) NOT NULL,
  `PositionId` int(11) NOT NULL,
  `WorkScheduleId` int(11) NOT NULL,
  `HiringDate` datetime DEFAULT NULL,
  `RecruitmentOrder` longtext,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_DepartmentId` (`DepartmentId` DESC) USING BTREE,
  KEY `IX_PositionId` (`PositionId` DESC) USING BTREE,
  KEY `IX_WorkScheduleId` (`WorkScheduleId` DESC) USING BTREE,
  KEY `FK_Employes_Departments_DepartmentId` (`DepartmentId`),
  KEY `FK_Employes_Positions_PositionId` (`PositionId`),
  KEY `FK_Employes_WorkSchedules_WorkScheduleId` (`WorkScheduleId`),
  CONSTRAINT `FK_Employes_Departments_DepartmentId` FOREIGN KEY (`DepartmentId`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Employes_Positions_PositionId` FOREIGN KEY (`PositionId`) REFERENCES `positions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Employes_WorkSchedules_WorkScheduleId` FOREIGN KEY (`WorkScheduleId`) REFERENCES `workschedules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1073 DEFAULT CHARSET=utf8;

CREATE TABLE `admissions` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeId` int(11) NOT NULL,
  `DeviceId` int(11) NOT NULL,
  `Token` longtext,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_EmployeId` (`EmployeId` DESC) USING BTREE,
  KEY `IX_DeviceId` (`DeviceId` DESC) USING BTREE,
  KEY `FK_Admissions_Devices_DeviceId` (`DeviceId`),
  KEY `FK_Admissions_Employes_EmployeId` (`EmployeId`),
  CONSTRAINT `FK_Admissions_Devices_DeviceId` FOREIGN KEY (`DeviceId`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Admissions_Employes_EmployeId` FOREIGN KEY (`EmployeId`) REFERENCES `employes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=630 DEFAULT CHARSET=utf8;

CREATE TABLE `holidays` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` longtext,
  `HolydayDate` datetime NOT NULL,
  `HolydayEndDate` datetime DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` longtext,
  `Password` longtext,
  `Salt` longtext,
  `LastLogin` datetime DEFAULT NULL,
  `Role` int(11) NOT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `visits` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Event` int(11) NOT NULL,
  `Code` longtext,
  `EmployeId` int(11) NOT NULL,
  `DepartmentId` int(11) NOT NULL,
  `DeviceId` int(11) NOT NULL,
  `RegisteredAt` datetime NOT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_EmployeId` (`EmployeId` DESC) USING BTREE,
  KEY `IX_DepartmentId` (`DepartmentId` DESC) USING BTREE,
  KEY `IX_DeviceId` (`DeviceId` DESC) USING BTREE,
  KEY `FK_Visits_Departments_DepartmentId` (`DepartmentId`),
  KEY `FK_Visits_Devices_DeviceId` (`DeviceId`),
  KEY `FK_Visits_Employes_EmployeId` (`EmployeId`),
  CONSTRAINT `FK_Visits_Departments_DepartmentId` FOREIGN KEY (`DepartmentId`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Visits_Devices_DeviceId` FOREIGN KEY (`DeviceId`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Visits_Employes_EmployeId` FOREIGN KEY (`EmployeId`) REFERENCES `employes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;