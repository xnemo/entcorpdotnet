﻿using EntCorp.Contracts.Repository;
using EntCorp.Data;
using EntCorp.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EntCorp.Repository
{
    public class DepartmentRepository : RepositoryBase<Department>, IDepartmentRepository
    {
        public DepartmentRepository(EntCorpContext context)
            : base(context)
        {
        }

        public IEnumerable<Department> GetAllDepartments()
        {
            return FindAll()
                .Where(d => d.Deleted != true)
                .OrderBy(d => d.Id)
                .ToList();
        }

        public IEnumerable<Department> GetAllDepartmentsWithWorkSchedule()
        {
            return FindAll()
                .Include(d => d.WorkSchedule)
                .Where(d => d.Deleted != true)
                .OrderBy(d => d.Name).ToList();
        }

        public Department GetDepartmentById(int Id)
        {
            return FindByCondition(d => d.Id.Equals(Id))
                .FirstOrDefault();
        }

        public Department GetDepartmentByName(string Name)
        {
            return FindByCondition(d => d.Name.Equals(Name))
                .FirstOrDefault();
        }

        public void CreateDepartment(Department department)
        {
            Create(department);
        }

        public void UpdateDepartment(Department department)
        {
            Update(department);
        }

        public void RemoveDepartment(Department department)
        {
            Delete(department);
        }
    }
}
