﻿using EntCorp.Contracts.Repository;
using EntCorp.Data;
using EntCorp.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace EntCorp.Repository
{
    public class HolidayRepository : RepositoryBase<Holiday>, IHolidayRepository
    {
        public HolidayRepository(EntCorpContext context)
            : base(context)
        {
        }

        public IEnumerable<Holiday> GetAllHolidays()
        {
            return FindAll()
                .Where(h => h.Deleted != true)
                .OrderBy(h => h.Id)
                .ToList();
        }

        public Holiday GetHolidayById(int Id)
        {
            return FindByCondition(h => h.Id.Equals(Id))
                .FirstOrDefault();
        }

        public void CreateHoliday(Holiday holiday)
        {
            Create(holiday);
        }

        public void UpdateHoliday(Holiday holiday)
        {
            Update(holiday);
        }

        public void RemoveHoliday(Holiday holiday)
        {
            Delete(holiday);
        }
    }
}
