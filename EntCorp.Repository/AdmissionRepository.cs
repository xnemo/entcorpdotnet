﻿using EntCorp.Contracts.Repository;
using EntCorp.Data;
using EntCorp.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EntCorp.Repository
{
    public class AdmissionRepository :RepositoryBase<Admission>, IAdmissionRepository
    {
        public AdmissionRepository(EntCorpContext context) 
            : base(context)
        {
        }

        public IEnumerable<Admission> GetAllAdmissions()
        {
            return FindAll()
                .Where(a => a.Deleted != true)
                .OrderBy(a => a.Id)
                .ToList();
        }

        public Admission GetAdmissionById(int Id)
        {
            return FindByCondition(a => a.Id.Equals(Id))
                .FirstOrDefault();
        }

        public IEnumerable<Admission> GetAdmissionsByDevice(int DeviceId)
        {
            return FindAll()
                .Include(a => a.Employe)
                .Where(a => a.EmployeId != null)
                .OrderBy(a => a.EmployeId)
                .ToList();
        }

        public IEnumerable<Admission> GetAdmissionsByEmploye(int EmployeId)
        {
            return FindAll()
                   .Where(a => a.EmployeId == EmployeId && a.Deleted != true)
                   .OrderBy(a => a.EmployeId)
                   .ToList();
        }

        public void CreateAdmission(Admission admission)
        {
            Create(admission);
        }

        public void UpdateAdmission(Admission admission)
        {
            Update(admission);
        }

        public void RemoveAdmission(Admission admission)
        {
            Delete(admission);
        }
    }
}
