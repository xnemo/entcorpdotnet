﻿using EntCorp.Contracts.Repository;
using EntCorp.Data;
using EntCorp.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace EntCorp.Repository
{
    public class WorkScheduleRepository : RepositoryBase<WorkSchedule>, IWorkScheduleRepository
    {
        public WorkScheduleRepository(EntCorpContext context)
            : base(context)
        {
        }

        public IEnumerable<WorkSchedule> GetAllWorkSchedules()
        {
            return FindAll()
                .Where(w => w.Deleted != true)
                .OrderBy(w => w.Id)
                .ToList();
        }

        public WorkSchedule GetWorkScheduleById(int Id)
        {
            return FindByCondition(w => w.Id.Equals(Id))
                .FirstOrDefault();
        }

        public void CreateWorkSchedule(WorkSchedule workSchedule)
        {
            Create(workSchedule);
        }

        public void UpdateWorkSchedule(WorkSchedule workSchedule)
        {
            Update(workSchedule);
        }

        public void RemoveWorkSchedule(WorkSchedule workSchedule)
        {
            Delete(workSchedule);
        }
    }
}
