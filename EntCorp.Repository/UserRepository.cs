﻿using EntCorp.Contracts.Repository;
using EntCorp.Data;
using EntCorp.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace EntCorp.Repository
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(EntCorpContext context)
            : base(context)
        {
        }

        public IEnumerable<User> GetAllUsers()
        {
            return FindAll()
                .Where(u => u.Deleted != true)
                .OrderBy(u => u.Id)
                .ToList();
        }

        public User GetUserById(int Id)
        {
            return FindByCondition(u => u.Id.Equals(Id))
                .FirstOrDefault();
        }

        public User GetUserByUsername(string Username)
        {
            return FindByCondition(u => u.Username.Equals(Username) && u.Deleted != true)
                .FirstOrDefault();
        }

        public void CreateUser(User user)
        {
            Create(user);
        }

        public void UpdateUser(User user)
        {
            Update(user);
        }

        public void RemoveUser(User user)
        {
            Delete(user);
        }
    }
}
