﻿using EntCorp.Contracts.Repository;
using EntCorp.Data;
using EntCorp.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EntCorp.Repository
{
    public class EmployeRepository : RepositoryBase<Employe>, IEmployeRepository
    {
        public EmployeRepository(EntCorpContext context)
            : base(context)
        {
        }

        public IEnumerable<Employe> GetAllEmployes()
        {
            return FindAll()
                .Where(e => e.Deleted != true)
                .OrderBy(e => e.Id)
                .ToList();
        }

        public IEnumerable<Employe> GetAllEmployesWithDepartments()
        {
            return FindAll()
                .Include(e => e.Department)
                .Include(e => e.WorkSchedule)
                .Include(e => e.Admissions)
                .Where(e => e.Deleted != true)
                .OrderBy(e => e.Fullname).ToList();
        }

        public Employe GetEmployeById(int Id)
        {
            return FindByCondition(e => e.Id.Equals(Id))
                .Include(e => e.Visits)
                .ThenInclude(v => v.Device)
                .FirstOrDefault();
        }

        public Employe GetEmployeByCode(string Code)
        {
            return FindByCondition(e => e.Code.Equals(Code) && e.Deleted != true)
                .FirstOrDefault();
        }

        public void CreateEmploye(Employe employe)
        {
            Create(employe);
        }

        public void UpdateEmploye(Employe employe)
        {
            Update(employe);
        }

        public void RemoveEmploye(Employe employe)
        {
            Delete(employe);
        }

        public IEnumerable<Employe> GetEmployeForReport(DateTime registeredAt)
        {
            return FindByCondition(e => e.Deleted != true)
                .Include(e => e.Department)
                .Include(e => e.WorkSchedule)
                .Select(e => new {
                    Employe = e,
                    Department = e.Department.Name,
                    WorkScheduleType = e.WorkSchedule.Type,
                    Visits = e.Visits.Where(v => 
                    v.RegisteredAt.Day == registeredAt.Day &&
                    v.RegisteredAt.Month == registeredAt.Month &&
                    v.RegisteredAt.Year == registeredAt.Year
                    )
                })
                .AsEnumerable()
                .Select(x => x.Employe)
                .ToList();

        }

        public IEnumerable<Employe> GetEmployeByDepartment(int DepartmentId)
        {
            return FindAll()
                .Include(e => e.Department)
                .Where(e => e.Deleted != true && e.Department.Id == DepartmentId)
                .OrderBy(e => e.Fullname)
                .ToList();
        }
    }
}
