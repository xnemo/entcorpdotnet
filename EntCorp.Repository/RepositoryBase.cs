﻿using EntCorp.Contracts;
using EntCorp.Data;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace EntCorp.Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected EntCorpContext EntCorpContext { get; set; }

        public RepositoryBase(EntCorpContext entCorpContext)
        {
            this.EntCorpContext = entCorpContext;
        }

        public IQueryable<T> FindAll()
        {
            return this.EntCorpContext.Set<T>();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.EntCorpContext.Set<T>().Where(expression);
        }

        public void Create(T entity)
        {
            this.EntCorpContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            this.EntCorpContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            this.EntCorpContext.Set<T>().Remove(entity);
        }       
    }
}
