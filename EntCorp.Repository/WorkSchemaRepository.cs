﻿using EntCorp.Contracts.Repository;
using EntCorp.Data;
using EntCorp.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace EntCorp.Repository
{
    public class WorkSchemaRepository : RepositoryBase<WorkSchema>, IWorkSchemaRepository
    {
        public WorkSchemaRepository(EntCorpContext context)
            : base(context)
        {
        }

        public IEnumerable<WorkSchema> GetAllWorkSchemas()
        {
            return FindAll()
                .Where(w => w.Deleted != true)
                .OrderBy(w => w.Id)
                .ToList();
        }

        public WorkSchema GetWorkSchemaById(int Id)
        {
            return FindByCondition(w => w.Id.Equals(Id))
                .FirstOrDefault();
        }

        public void CreateWorkSchema(WorkSchema workSchema)
        {
            Create(workSchema);
        }

        public void UpdateWorkSchema(WorkSchema workSchema)
        {
            Update(workSchema);
        }

        public void RemoveWorkSchema(WorkSchema workSchema)
        {
            Delete(workSchema);
        }

        public WorkSchema GetWorkSchemaByWorkScheduleAndDay(int workScheduleId, int Day)
        {
            return FindByCondition(w => w.WorkScheduleId == workScheduleId && w.DayNumber == Day && w.Deleted != true)
                .FirstOrDefault();
        }

        public IEnumerable<WorkSchema> GetWorkSchemaBySchedule(int workScheduleId)
        {
            return FindByCondition(w => w.WorkScheduleId == workScheduleId && w.Deleted != true)
                .ToList();
        } 
    }
}
