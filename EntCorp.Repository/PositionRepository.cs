﻿using EntCorp.Contracts.Repository;
using EntCorp.Data;
using EntCorp.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace EntCorp.Repository
{
    public class PositionRepository : RepositoryBase<Position>, IPositionRepository
    {
        public PositionRepository(EntCorpContext context)
            : base(context)
        {
        }

        public IEnumerable<Position> GetAllPositions()
        {
            return FindAll()
                .Where(p => p.Deleted != true)
                .OrderBy(p => p.Id)
                .ToList();
        }

        public Position GetPositionById(int Id)
        {
            return FindByCondition(p => p.Id.Equals(Id))
                .FirstOrDefault();
        }

        public void CreatePosition(Position position)
        {
            Create(position);
        }

        public void UpdatePosition(Position position)
        {
            Update(position);
        }

        public void RemovePosition(Position position)
        {
            Delete(position);
        }
    }
}
