﻿using EntCorp.Contracts;
using EntCorp.Contracts.Repository;
using EntCorp.Data;

namespace EntCorp.Repository
{
    public class IRepositoryWrapper : Contracts.IRepositoryWrapper
    {
        private EntCorpContext _entCorpContext;
        private IAdmissionRepository _admission;
        private IDepartmentRepository _department;
        private IDeviceRepository _device;
        private IEmployeRepository _employe;
        private IHolidayRepository _holiday;
        private IPositionRepository _position;
        private IUserRepository _user;
        private IVisitRepository _visit;
        private IWorkScheduleRepository _workSchedule;
        private IWorkSchemaRepository _workSchema;

        public IRepositoryWrapper(EntCorpContext entCorpContext)
        {
            _entCorpContext = entCorpContext;
        }

        public IAdmissionRepository Admission => _admission ?? new AdmissionRepository(_entCorpContext);
        public IDepartmentRepository Department => _department ?? new DepartmentRepository(_entCorpContext);
        public IDeviceRepository Device => _device ?? new DeviceRepository(_entCorpContext);
        public IEmployeRepository Employe => _employe ?? new EmployeRepository(_entCorpContext);
        public IHolidayRepository Holiday => _holiday ?? new HolidayRepository(_entCorpContext);
        public IPositionRepository Position => _position ?? new PositionRepository(_entCorpContext);
        public IUserRepository User => _user ?? new UserRepository(_entCorpContext);
        public IVisitRepository Visit => _visit ?? new VisitRepository(_entCorpContext);
        public IWorkScheduleRepository WorkSchedule => _workSchedule ?? new WorkScheduleRepository(_entCorpContext);
        public IWorkSchemaRepository WorkSchema => _workSchema ?? new WorkSchemaRepository(_entCorpContext);
        public void Save() => _entCorpContext.SaveChanges();
        
    }
}
