﻿using EntCorp.Contracts.Repository;
using EntCorp.Data;
using EntCorp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EntCorp.Repository
{
    public class VisitRepository : RepositoryBase<Visit>, IVisitRepository
    {
        EntCorpContext _context;
        public VisitRepository(EntCorpContext context)
            : base(context)
        {
            _context = context;
        }

        public IEnumerable<Visit> GetAllVisits()
        {
            return FindAll()
                .Where(v => v.Deleted != true)
                .OrderBy(v => v.Id)
                .ToList();
        }

        public Visit GetVisitById(int Id)
        {
            return FindByCondition(v => v.Id.Equals(Id))
                .FirstOrDefault();
        }

        public Visit GetLastVisitByDevice(int DeviceId)
        {
            return FindByCondition(v => v.DeviceId == DeviceId)
                .OrderByDescending(v => v.RegisteredAt)
                .FirstOrDefault();
        }

        public void CreateVisit(Visit visit)
        {
            Create(visit);
        }

        public void UpdateVisit(Visit visit)
        {
            Update(visit);
        }

        public void RemoveVisit(Visit visit)
        {
            Delete(visit);
        }

        public IEnumerable<Visit> GetVisitsByPersonAndDate(int EmployeId, DateTime dateTime)
        {
            return FindAll()
                .Where(v => v.EmployeId == EmployeId 
                    && v.RegisteredAt.Day == dateTime.Day
                    && v.RegisteredAt.Month == dateTime.Month
                    && v.RegisteredAt.Year == dateTime.Year
                    )
                .OrderBy(v => v.Id)
                .ToList();
        }
    }
}
