﻿using EntCorp.Contracts.Repository;
using EntCorp.Data;
using EntCorp.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EntCorp.Repository
{
    public class DeviceRepository : RepositoryBase<Device>, IDeviceRepository
    {
        public DeviceRepository(EntCorpContext context)
            : base(context)
        {
        }

        public IEnumerable<Device> GetAllDevices()
        {
            return FindAll()
                .Where(d => d.Deleted != true)
                .OrderBy(d => d.Id)
                .ToList();
        }

        public IEnumerable<Device> GetDevicesAsNoTracking()
        {
            return FindAll()
                .Where(d => d.Deleted != true)
                .OrderBy(d => d.Id)
                .AsNoTracking()
                .ToList();
        }

        public Device GetDeviceById(int Id)
        {
            return FindByCondition(d => d.Id.Equals(Id))
                .FirstOrDefault();
        }

        public void CreateDevice(Device device)
        {
            Create(device);
        }

        public void UpdateDevice(Device device)
        {
            Update(device);
        }

        public void RemoveDevice(Device device)
        {
            Delete(device);
        }
    }
}
