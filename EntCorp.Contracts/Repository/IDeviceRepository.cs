﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Repository
{
    public interface IDeviceRepository : IRepositoryBase<Device>
    {
        IEnumerable<Device> GetAllDevices();
        IEnumerable<Device> GetDevicesAsNoTracking();
        Device GetDeviceById(int Id);
        void CreateDevice(Device device);
        void UpdateDevice(Device device);
        void RemoveDevice(Device device);
    }
}
