﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Repository
{
    public interface IDepartmentRepository : IRepositoryBase<Department>
    {
        IEnumerable<Department> GetAllDepartments();
        IEnumerable<Department> GetAllDepartmentsWithWorkSchedule();
        Department GetDepartmentById(int Id);
        Department GetDepartmentByName(string Name);
        void CreateDepartment(Department department);
        void UpdateDepartment(Department department);
        void RemoveDepartment(Department department);
    }
}
