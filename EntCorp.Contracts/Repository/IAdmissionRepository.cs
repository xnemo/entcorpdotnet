﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Repository
{
    public interface IAdmissionRepository : IRepositoryBase<Admission>
    {
        IEnumerable<Admission> GetAllAdmissions();
        IEnumerable<Admission> GetAdmissionsByDevice(int DeviceId);
        IEnumerable<Admission> GetAdmissionsByEmploye(int EmployeId);
        Admission GetAdmissionById(int Id);
        void CreateAdmission(Admission admission);
        void UpdateAdmission(Admission admission);
        void RemoveAdmission(Admission admission);
    }
}
