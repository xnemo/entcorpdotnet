﻿using EntCorp.Data.Models;
using System;
using System.Collections.Generic;

namespace EntCorp.Contracts.Repository
{
    public interface IVisitRepository : IRepositoryBase<Visit>
    {
        IEnumerable<Visit> GetAllVisits();
        Visit GetVisitById(int Id);
        Visit GetLastVisitByDevice(int DeviceId);
        void CreateVisit(Visit visit);
        void UpdateVisit(Visit visit);
        void RemoveVisit(Visit visit);
        IEnumerable<Visit> GetVisitsByPersonAndDate(int EmployeId, DateTime dateTime);
    }
}
