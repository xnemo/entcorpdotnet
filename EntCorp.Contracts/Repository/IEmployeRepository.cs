﻿using EntCorp.Data.Models;
using System;
using System.Collections.Generic;

namespace EntCorp.Contracts.Repository
{
    public interface IEmployeRepository : IRepositoryBase<Employe>
    {
        IEnumerable<Employe> GetAllEmployes();
        IEnumerable<Employe> GetAllEmployesWithDepartments();
        IEnumerable<Employe> GetEmployeForReport(DateTime registeredAt);
        IEnumerable<Employe> GetEmployeByDepartment(int DepartmentId);
        Employe GetEmployeById(int Id);
        Employe GetEmployeByCode(string Code);
        void CreateEmploye(Employe employe);
        void UpdateEmploye(Employe employe);
        void RemoveEmploye(Employe employe);
    }
}
