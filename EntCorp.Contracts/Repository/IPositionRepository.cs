﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Repository
{
    public interface IPositionRepository : IRepositoryBase<Position>
    {
        IEnumerable<Position> GetAllPositions();
        Position GetPositionById(int Id);
        void CreatePosition(Position position);
        void UpdatePosition(Position position);
        void RemovePosition(Position position);
    }
}
