﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Repository
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        IEnumerable<User> GetAllUsers();
        User GetUserById(int Id);
        User GetUserByUsername(string Username);
        void CreateUser(User user);
        void UpdateUser(User user);
        void RemoveUser(User user);
    }
}
