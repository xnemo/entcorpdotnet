﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Repository
{
    public interface IWorkSchemaRepository : IRepositoryBase<WorkSchema>
    {
        IEnumerable<WorkSchema> GetAllWorkSchemas();
        WorkSchema GetWorkSchemaById(int Id);
        WorkSchema GetWorkSchemaByWorkScheduleAndDay(int workScheduleId, int Day);
        IEnumerable<WorkSchema> GetWorkSchemaBySchedule(int workScheduleId);
        void CreateWorkSchema(WorkSchema workSchema);
        void UpdateWorkSchema(WorkSchema workSchema);
        void RemoveWorkSchema(WorkSchema workSchema);
    }
}
