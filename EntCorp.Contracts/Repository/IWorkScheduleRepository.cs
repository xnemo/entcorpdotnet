﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Repository
{
    public interface IWorkScheduleRepository : IRepositoryBase<WorkSchedule>
    {
        IEnumerable<WorkSchedule> GetAllWorkSchedules();
        WorkSchedule GetWorkScheduleById(int Id);
        void CreateWorkSchedule(WorkSchedule workSchedule);
        void UpdateWorkSchedule(WorkSchedule workSchedule);
        void RemoveWorkSchedule(WorkSchedule workSchedule);
    }
}
