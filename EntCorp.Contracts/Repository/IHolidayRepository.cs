﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Repository
{
    public interface IHolidayRepository : IRepositoryBase<Holiday>
    {
        IEnumerable<Holiday> GetAllHolidays();
        Holiday GetHolidayById(int Id);
        void CreateHoliday(Holiday holiday);
        void UpdateHoliday(Holiday holiday);
        void RemoveHoliday(Holiday holiday);
    }
}
