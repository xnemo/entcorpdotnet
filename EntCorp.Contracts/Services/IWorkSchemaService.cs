﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Services
{
    public interface IWorkSchemaService
    {
        IEnumerable<WorkSchema> GetAllWorkSchemas();
        WorkSchema GetWorkSchemaById(int Id);
        void CreateWorkSchema(WorkSchema workSchema);
        void UpdateWorkSchema(WorkSchema workSchema);
        void RemoveWorkSchema(WorkSchema workSchema);
    }
}
