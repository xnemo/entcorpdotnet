﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Services
{
    public interface IHolidayService
    {
        IEnumerable<Holiday> GetAllHolidays();
        Holiday GetHolidayById(int Id);
        void CreateHoliday(Holiday holiday);
        void UpdateHoliday(Holiday holiday);
        void RemoveHoliday(Holiday holiday);
    }
}
