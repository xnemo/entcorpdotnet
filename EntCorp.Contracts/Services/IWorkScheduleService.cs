﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Services
{
    public interface IWorkScheduleService
    {
        IEnumerable<WorkSchedule> GetAllWorkSchedules();
        WorkSchedule GetWorkScheduleById(int Id);
        void CreateWorkSchedule(WorkSchedule workSchedule);
        void UpdateWorkSchedule(WorkSchedule workSchedule);
        void RemoveWorkSchedule(WorkSchedule workSchedule);
    }
}
