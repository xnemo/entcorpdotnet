﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Services
{
    public interface IAdmissionService
    {
        IEnumerable<Admission> GetAllAdmissions();
        Admission GetAdmissionById(int Id);
        void CreateAdmission(Admission admission);
        void UpdateAdmission(Admission admission);
        void RemoveAdmission(Admission admission);
    }
}
