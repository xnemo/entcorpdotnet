﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Services
{
    public interface IVisitService
    {
        IEnumerable<Visit> GetAllVisits();
        Visit GetVisitById(int Id);
        void CreateVisit(Visit visit);
        void UpdateVisit(Visit visit);
        void RemoveVisit(Visit visit);
    }
}
