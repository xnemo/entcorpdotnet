﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Services
{
    public interface IPositionService
    {
        IEnumerable<Position> GetAllPositions();
        Position GetPositionById(int Id);
        void CreatePosition(Position position);
        void UpdatePosition(Position position);
        void RemovePosition(Position position);
    }
}
