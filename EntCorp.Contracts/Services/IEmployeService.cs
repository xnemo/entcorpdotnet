﻿using EntCorp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntCorp.Contracts.Services
{
    public interface IEmployeService
    {
        IEnumerable<Employe> GetAllEmployes();
        IEnumerable<Employe> GetAllEmployesWithDepartments();
        Employe GetEmployeById(int Id);
        void CreateEmploye(Employe employe);
        void UpdateEmploye(Employe employe);
        void RemoveEmploye(Employe employe);
    }
}
