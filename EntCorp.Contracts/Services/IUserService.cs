﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Services
{
    public interface IUserService
    {
        IEnumerable<User> GetAllUsers();
        User GetUserById(int Id);
        void CreateUser(User user);
        void UpdateUser(User user);
        void RemoveUser(User user);
    }
}
