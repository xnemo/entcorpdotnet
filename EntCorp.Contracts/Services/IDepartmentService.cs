﻿using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Contracts.Services
{
    public interface IDepartmentService
    {
        IEnumerable<Department> GetAllDepartments();
        Department GetDepartmentById(int Id);
        void CreateDepartment(Department department);
        void UpdateDepartment(Department department);
        void RemoveDepartment(Department department);
    }
}
