﻿using EntCorp.Contracts.Repository;
using System.Threading.Tasks;

namespace EntCorp.Contracts
{
    public interface IRepositoryWrapper
    {
        IAdmissionRepository Admission { get; }
        IDepartmentRepository Department { get; }
        IDeviceRepository Device { get; }
        IEmployeRepository Employe { get; }
        IHolidayRepository Holiday { get; }
        IPositionRepository Position { get; }
        IUserRepository User { get; }
        IVisitRepository Visit { get; }
        IWorkScheduleRepository WorkSchedule { get; }
        IWorkSchemaRepository WorkSchema { get; }
        void Save();
    }
}
