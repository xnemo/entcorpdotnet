﻿using EntCorp.Contracts;
using EntCorp.Contracts.Services;
using EntCorp.Data.Models;
using System;
using System.Collections.Generic;

namespace EntCorp.Service
{
    public class HolidayService : IHolidayService
    {
        private readonly IRepositoryWrapper _repoWrapper;
        public HolidayService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public void CreateHoliday(Holiday holiday)
        {
            _repoWrapper.Holiday.CreateHoliday(holiday);
            _repoWrapper.Save();
        }

        public IEnumerable<Holiday> GetAllHolidays()
        {
            return _repoWrapper.Holiday.GetAllHolidays();
        }

        public Holiday GetHolidayById(int Id)
        {
            return _repoWrapper.Holiday.GetHolidayById(Id);
        }

        public void RemoveHoliday(Holiday holiday)
        {
            _repoWrapper.Holiday.RemoveHoliday(holiday);
            _repoWrapper.Save();
        }

        public void UpdateHoliday(Holiday holiday)
        {
            _repoWrapper.Holiday.UpdateHoliday(holiday);
            _repoWrapper.Save();
        }
    }
}
