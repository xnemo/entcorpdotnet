﻿using EntCorp.Contracts;
using EntCorp.Contracts.Services;
using EntCorp.Data.Models;
using EntCorp.Utils;
using System.Collections.Generic;

namespace EntCorp.Service
{
    public class UserService : IUserService
    {
        private IRepositoryWrapper _repoWrapper;
        public UserService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public void CreateUser(User user)
        {
            user.Salt = TokenHelper.GetSalt(user.Password);
            user.Password = TokenHelper.GetSaltedPassword(user.Password, user.Salt);
            _repoWrapper.User.CreateUser(user);
            _repoWrapper.Save();
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _repoWrapper.User.GetAllUsers();
        }

        public User GetUserById(int Id)
        {
            return _repoWrapper.User.GetUserById(Id);
        }

        public User GetUserByUsername(string Username)
        {
            return _repoWrapper.User.GetUserByUsername(Username);
        }

        public void RemoveUser(User user)
        {
            _repoWrapper.User.RemoveUser(user);
            _repoWrapper.Save();
        }

        public void UpdateUser(User user)
        {
            _repoWrapper.User.UpdateUser(user);
            _repoWrapper.Save();
        }
    }
}
