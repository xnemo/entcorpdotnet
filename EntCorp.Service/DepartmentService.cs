﻿using EntCorp.Contracts;
using EntCorp.Contracts.Services;
using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Service
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IRepositoryWrapper _repoWrapper;
        public DepartmentService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public void CreateDepartment(Department department)
        {
            _repoWrapper.Department.CreateDepartment(department);
            _repoWrapper.Save();
        }

        public IEnumerable<Department> GetAllDepartments()
        {
            return _repoWrapper.Department.GetAllDepartments();            
        }

        public IEnumerable<Department> GetAllDepartmentsWithWorkSchedule()
        {
            return _repoWrapper.Department.GetAllDepartmentsWithWorkSchedule();
        }

        public Department GetDepartmentById(int Id)
        {
            return _repoWrapper.Department.GetDepartmentById(Id);
        }

        public Department GetDepartmentByName(string Name)
        {
            return _repoWrapper.Department.GetDepartmentByName(Name);
        }

        public void RemoveDepartment(Department department)
        {
            _repoWrapper.Department.RemoveDepartment(department);
            _repoWrapper.Save();
        }

        public void UpdateDepartment(Department department)
        {
            _repoWrapper.Department.UpdateDepartment(department);
            _repoWrapper.Save();
        }
    }
}
