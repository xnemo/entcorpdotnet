﻿using EntCorp.Contracts;
using EntCorp.Contracts.Services;
using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Service
{
    public class DeviceService : IDeviceService
    {
        private readonly IRepositoryWrapper _repoWrapper;
        public DeviceService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public IEnumerable<Device> GetAllDevices()
        {
            return _repoWrapper.Device.GetAllDevices();
        }

        public IEnumerable<Device> GetDevicesAsNoTracking()
        {
            return _repoWrapper.Device.GetDevicesAsNoTracking();
        }

        public Device GetDeviceById(int Id)
        {
            return _repoWrapper.Device.GetDeviceById(Id);
        }

        public void CreateDevice(Device device)
        {
            _repoWrapper.Device.CreateDevice(device);
            _repoWrapper.Save();
        }

        public void UpdateDevice(Device device)
        {
            _repoWrapper.Device.UpdateDevice(device);
            _repoWrapper.Save();
        }

        public void RemoveDevice(Device device)
        {
            _repoWrapper.Device.RemoveDevice(device);
            _repoWrapper.Save();
        }
    }
}
