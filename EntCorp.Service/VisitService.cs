﻿using EntCorp.Contracts;
using EntCorp.Contracts.Services;
using EntCorp.Data.Models;
using System;
using System.Collections.Generic;

namespace EntCorp.Service
{
    public class VisitService : IVisitService
    {
        private IRepositoryWrapper _repoWrapper;
        public VisitService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public void CreateVisit(Visit visit)
        {
            _repoWrapper.Visit.CreateVisit(visit);
            _repoWrapper.Save();
        }

        public IEnumerable<Visit> GetAllVisits()
        {
            return _repoWrapper.Visit.GetAllVisits();
        }

        public Visit GetVisitById(int Id)
        {
            return _repoWrapper.Visit.GetVisitById(Id);
        }

        public Visit GetLastVisitByDevice(int DeviceId)
        {
            return _repoWrapper.Visit.GetLastVisitByDevice(DeviceId);
        }

        public void RemoveVisit(Visit visit)
        {
            _repoWrapper.Visit.RemoveVisit(visit);
            _repoWrapper.Save();
        }

        public void UpdateVisit(Visit visit)
        {
            _repoWrapper.Visit.UpdateVisit(visit);
            _repoWrapper.Save();
        }

        public IEnumerable<Visit> GetVisitsByPersonAndDate(int EmployeId, DateTime dateTime)
        {
            return _repoWrapper.Visit.GetVisitsByPersonAndDate(EmployeId, dateTime);
        }
    }
}
