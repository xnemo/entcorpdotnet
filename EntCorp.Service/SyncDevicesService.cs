﻿using EntCorp.Contracts;
using EntCorp.Data.Models;
using EntCorp.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EntCorp.Service
{
    class SyncVisit
    {
        public string Code;
        public int Event;
        public int RegisteredAt;
        public string Path;
    }
    
    public class SyncDevicesService
    {
        private DeviceService _deviceService;
        private EmployeService _employeService;
        private VisitService _visitService;
        private AdmissionService _admissionService;

        public SyncDevicesService(IRepositoryWrapper repoWrapper)
        {
            _deviceService = new DeviceService(repoWrapper);
            _employeService = new EmployeService(repoWrapper);
            _visitService = new VisitService(repoWrapper);
            _admissionService = new AdmissionService(repoWrapper);
        }

        public string SendData()
        {
            string responseString = "";

            var devices = _deviceService.GetDevicesAsNoTracking();
            foreach (var d in devices)
            {
                switch (d.ReaderType)
                {
                    case ReaderTypes.RFID:
                        PostRfidList(d, out responseString);
                        break;
                    case ReaderTypes.NFC:
                        break;
                    case ReaderTypes.QR:
                        break;
                    case ReaderTypes.FINGERPRINT:
                        break;
                    case ReaderTypes.FACEID:
                        PostListToFaceTerminal(d, out responseString);
                        break;
                    default:
                        break;
                }
            }

            return responseString;
        }

        public async Task<string> GetData()
        {
            string responseString = "";
            var devices = _deviceService.GetDevicesAsNoTracking();
            foreach (var d in devices)
            {
                switch (d.ReaderType)
                {
                    case ReaderTypes.RFID:
                        responseString = await GetRfidEventsAsync(d);
                        ClearData(d);
                        break;
                    case ReaderTypes.NFC:
                        break;
                    case ReaderTypes.QR:
                        break;
                    case ReaderTypes.FINGERPRINT:
                        break;
                    case ReaderTypes.FACEID:
                        await Task.Factory.StartNew(
                            () => GetFaceTerminalEvents(d, out responseString), TaskCreationOptions.LongRunning);
                        break;
                    case ReaderTypes.GATE:
                        break;
                    default:
                        break;
                }
                Thread.Sleep(5000);
            }
            return responseString;
        }

        private async Task<string> GetRfidEventsAsync(Device d)
        {
            string lastRegisteredAt, result;
            try
            {
                string url = $"http://{d.IpAdress}/visits";
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("User-Agent", "EntCorpDesktop");
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json")); 
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetBaseToken(d.Token));
                    var response = await client.GetAsync(url);
                    var str = await response.Content.ReadAsStringAsync();
                    string json = String.Empty;
                    List<SyncVisit> visits = JsonConvert.DeserializeObject<List<SyncVisit>>(str);
                    foreach (var v in visits)
                    {
                        Employe emp = _employeService.GetEmployeByCode(v.Code);
                        if (emp == null)
                            continue;

                        Visit visit = new Visit
                        {
                            Code = v.Code,
                            DeviceId = d.Id,
                            Event = v.Event == 1 ? Visit.Events.IN : Visit.Events.OUT,
                            EmployeId = emp.Id,
                            DepartmentId = emp.DepartmentId,
                            RegisteredAt = TimeHelper.UnixTimeStampToDateTime(v.RegisteredAt)
                        };

                        _visitService.CreateVisit(visit);
                        lastRegisteredAt = v.RegisteredAt.ToString();
                        Logger.LogInfo($"{v.Code} - {v.Event} - {v.RegisteredAt}");
                    }

                    result = $"События с устройства {d.Name} получены";
                    Logger.LogInfo(result);
                };
            }
            catch (WebException webException)
            {
                result = webException.Message;
                Logger.LogError(webException.Message);
            }
            catch (Exception ex)
            {
                result = ex.Message;
                Logger.LogError(ex.Message);
            }
            Thread.Sleep(3000);
            return result;
        }

        private void GetFaceTerminalEvents(Device d, out string responseString)
        {
            string endpoint = d.IpAdress;
            string lastRegisteredAt = "1970-01-01 00:00:00";
            Visit lastVisit = _visitService.GetLastVisitByDevice(d.Id);
            if (lastVisit != null)
                lastRegisteredAt = lastVisit.RegisteredAt.ToString("yyyy-MM-dd HH:mm:ss");

            DateTime localDateTime = DateTime.Parse(lastRegisteredAt);
            var utcDateTime = localDateTime.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss");

            try
            {
                WebRequest request = WebRequest.Create($"http://{endpoint}/api/v1/visits?updates_from={utcDateTime}");
                request.ContentType = "application/json; charset=UTF-8";
                string token = GetBaseToken(d.Token);
                request.Headers.Add("Authorization", $"Bearer {token}");

                request.UseDefaultCredentials = true;
                request.PreAuthenticate = true;
                request.Credentials = CredentialCache.DefaultCredentials;

                WebResponse response = request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string str = reader.ReadLine();
                string json = String.Empty;

                while (str != null)
                {
                    json = String.Concat(json, str);
                    str = reader.ReadLine();
                }

                List<SyncVisit> visits = JsonConvert.DeserializeObject<List<SyncVisit>>(json);

                foreach (var v in visits)
                {
                    Employe emp = _employeService.GetEmployeByCode(v.Code);

                    if (emp == null)
                        continue;

                    string directory = @"c:\EntCorporate\visits";
                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    directory = Path.Combine(directory, emp.Id.ToString());

                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    string path = Path.Combine(directory, DateTime.Now.Ticks.ToString() + ".jpg");

                    if (String.IsNullOrEmpty(v.Path) || String.IsNullOrWhiteSpace(v.Path))
                        continue;

                    WebClient webClient = new WebClient();
                    webClient.DownloadFile($"http://{endpoint}{v.Path}", path);

                    Visit visit = new Visit
                    {
                        Code = v.Code,
                        DeviceId = d.Id,
                        Event = v.Event == 1 ? Visit.Events.IN : Visit.Events.OUT,
                        EmployeId = emp.Id,
                        DepartmentId = emp.DepartmentId,
                        RegisteredAt = TimeHelper.UnixTimeStampToDateTime(v.RegisteredAt),
                        Photo = path
                    };

                    _visitService.CreateVisit(visit);
                    lastRegisteredAt = v.RegisteredAt.ToString();
                    Logger.LogInfo($"{v.Code} - {v.Event} - {v.RegisteredAt}");
                }
                
                reader.Close();
                response.Close();
                responseString = $"События с устройства {d.Name} получены";
            }
            catch (WebException webException)
            {
                responseString = webException.Message;
                Logger.LogError(webException.Message);
            }
            catch (Exception ex)
            {
                responseString = ex.Message;
                Logger.LogError(ex.Message);
            }
            Thread.Sleep(1000);
            responseString = "";
        }

        public string ClearData(Device d)
        {
            string result = "";
            try
            {
                var deviceLastVisit = _visitService.GetLastVisitByDevice(d.Id);

                var timestamp = TimeHelper.ConvertToUnixTimestamp(deviceLastVisit.RegisteredAt);

                WebRequest request = WebRequest.Create($"http://{d.IpAdress}/visits?lastRegisteredAt={timestamp.ToString()}");
                request.Method = "DELETE";

                request.ContentType = "application/json; charset=UTF-8";
                request.Headers.Add("Authorization", $"Basic {GetBaseToken(d.Token)}");
                WebResponse response = request.GetResponse();
                response.Close();
                result = $"События с устройства {d.Name} удалены";
                Logger.LogInfo(result);
            }
            catch (WebException webException)
            {
                Logger.LogError(webException.Message);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
            return result;
        }

        private string GetBaseToken(string token)
        {
            var textBytes = Encoding.UTF8.GetBytes($"corp:{token}");
            return Convert.ToBase64String(textBytes);
        }

        private void PostRfidList(Device d, out string responseString)
        {
            JArray array = new JArray();
            string endpoint = $"http://{d.IpAdress}/keys";

            var admissions = _admissionService.GetAdmissionsByDevice(d.Id);

            foreach (var a in admissions)
            {
                if (!a.Employe.Deleted.GetValueOrDefault())
                    array.Add(a.Token);
            }

            JObject o = new JObject();
            o["data"] = array;
            responseString = o.ToString();

            WebRequest request = WebRequest.Create(endpoint);
            request.Method = "POST";
            byte[] byteArray = Encoding.UTF8.GetBytes(responseString);
            request.ContentType = "application/json; charset=UTF-8";
            request.Headers.Add("Authorization", $"Basic {GetBaseToken(d.Token)}");
            request.ContentLength = byteArray.Length;

            try
            {
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse response = request.GetResponse();
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                responseString = $"Данные отправлены на устройство {d.Name}";
            }
            catch (WebException webException)
            {
                responseString = webException.Message;
                Logger.LogError(webException.Message);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
            Thread.Sleep(1000);
        }

        private void PostListToFaceTerminal(Device d, out string responseString)
        {
            responseString = "";
            string endpoint = $"http://{d.IpAdress}/api/v1/cards";
            var admissions = _admissionService.GetAdmissionsByDevice(d.Id);

            var items = admissions.GroupBy(a => a.EmployeId)
                .Select(a => a.OrderBy(b => b.CreatedAt).Last());

            foreach (var a in items)
            {
                Logger.LogInfo($"Отправка {a.Employe.Id} - {a.Employe.Fullname} - {a.Deleted} - {a.Id} на устройство");
                using (var client = new HttpClient())
                {
                    using (var formData = new MultipartFormDataContent())
                    {
                        formData.Add(new StringContent(a.Employe.Fullname), "full_name");
                        formData.Add(new StringContent(a.Employe.Position), "info");
                        formData.Add(new StringContent(a.Token), "code");
                        formData.Add(new StringContent(a.Deleted.ToString()), "deleted");
                        if (!String.IsNullOrEmpty(a.Employe.Photo) && File.Exists(a.Employe.Photo))
                        {
                            var streamContent = new StreamContent(File.OpenRead(a.Employe.Photo));
                            streamContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                            formData.Add(streamContent, "Photo", "photo.jpg");
                            try
                            {
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", GetBaseToken(d.Token));
                                var response = client.PostAsync(endpoint, formData).Result;
                                Logger.LogInfo(response.ReasonPhrase);
                                if (response.IsSuccessStatusCode)
                                {
                                    responseString = $"Данные отправлены на устройство {d.Name}";
                                }
                                else
                                {
                                    responseString = $"Неправильный ответ от сервера {d.Name}";
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LogError(ex.Message);
                            }
                            finally
                            {
                                client.CancelPendingRequests();
                            }
                        }
                    }
                }
            }
            Logger.LogDebug("---------------------------------------------");
            Logger.LogDebug("----------Конец отправки---------------------");
        }
    }
}
