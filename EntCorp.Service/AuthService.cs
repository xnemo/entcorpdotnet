﻿using EntCorp.Data.Models;
using EntCorp.Utils;
using System;

namespace EntCorp.Service
{
    public class AuthService
    {
        private UserService _userService;

        public AuthService(UserService userService)
        {
            _userService = userService;
        }

        public bool Auth(string username, string password, out string result)
        {
            //try
            {
                User user = _userService.GetUserByUsername(username);
                if (user == null)
                {
                    result = "Пользователь не найден";
                    return false;
                }

                if (TokenHelper.GetSaltedPassword(password, user.Salt) == user.Password)
                {
                    result = user.Username;
                    return true;
                }

                result = "Неправильный пароль";
                return false;
            }
            //catch (Exception ex)
            //{
            //    Logger.LogError(ex.Message);
            //    result = "Ошибка выполнения";
            //    return false;
            //}
        }
    }
}
