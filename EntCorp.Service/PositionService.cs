﻿using EntCorp.Contracts;
using EntCorp.Contracts.Services;
using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Service
{
    class PositionService : IPositionService
    {
        private IRepositoryWrapper _repoWrapper;
        public PositionService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public void CreatePosition(Position position)
        {
            _repoWrapper.Position.CreatePosition(position);
            _repoWrapper.Save();
        }

        public IEnumerable<Position> GetAllPositions()
        {
            return _repoWrapper.Position.GetAllPositions();
        }

        public Position GetPositionById(int Id)
        {
            return _repoWrapper.Position.GetPositionById(Id);
        }

        public void RemovePosition(Position position)
        {
            _repoWrapper.Position.RemovePosition(position);
            _repoWrapper.Save();
        }

        public void UpdatePosition(Position position)
        {
            _repoWrapper.Position.UpdatePosition(position);
            _repoWrapper.Save();
        }
    }
}
