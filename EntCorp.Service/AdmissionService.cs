﻿using System.Collections.Generic;
using EntCorp.Contracts;
using EntCorp.Contracts.Services;
using EntCorp.Data.Models;

namespace EntCorp.Service
{
    public class AdmissionService : IAdmissionService
    {
        private readonly IRepositoryWrapper _repoWrapper;
        public AdmissionService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public void CreateAdmission(Admission admission)
        {
            _repoWrapper.Admission.CreateAdmission(admission);
            _repoWrapper.Save();
        }

        public Admission GetAdmissionById(int Id)
        {
            return _repoWrapper.Admission.GetAdmissionById(Id);
        }

        public IEnumerable<Admission> GetAllAdmissions()
        {
            return _repoWrapper.Admission.GetAllAdmissions();
        }

        public IEnumerable<Admission> GetAdmissionsByDevice(int DeviceId)
        {
            return _repoWrapper.Admission.GetAdmissionsByDevice(DeviceId);
        }

        public IEnumerable<Admission> GetAdmissionsByEmploye(int EmployeId)
        {
            return _repoWrapper.Admission.GetAdmissionsByEmploye(EmployeId);
        }

        public void RemoveAdmission(Admission admission)
        {
            _repoWrapper.Admission.RemoveAdmission(admission);
        }

        public void UpdateAdmission(Admission admission)
        {
            _repoWrapper.Admission.UpdateAdmission(admission);
        }
    }
}
