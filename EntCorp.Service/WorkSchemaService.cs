﻿using EntCorp.Contracts;
using EntCorp.Contracts.Services;
using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Service
{
    public class WorkSchemaService : IWorkSchemaService
    {
        private readonly IRepositoryWrapper _repoWrapper;
        public WorkSchemaService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public void CreateWorkSchema(WorkSchema workSchema)
        {
            _repoWrapper.WorkSchema.CreateWorkSchema(workSchema);
            _repoWrapper.Save();
        }

        public IEnumerable<WorkSchema> GetAllWorkSchemas()
        {
            return _repoWrapper.WorkSchema.GetAllWorkSchemas();
        }

        public WorkSchema GetWorkSchemaById(int Id)
        {
            return _repoWrapper.WorkSchema.GetWorkSchemaById(Id);
        }

        public void RemoveWorkSchema(WorkSchema workSchema)
        {
            _repoWrapper.WorkSchema.RemoveWorkSchema(workSchema);
            _repoWrapper.Save();
        }

        public void UpdateWorkSchema(WorkSchema workSchema)
        {
            _repoWrapper.WorkSchema.UpdateWorkSchema(workSchema);
            _repoWrapper.Save();
        }

        public WorkSchema GetWorkSchemaByWorkScheduleAndDay(int workScheduleId, int Day)
        {
            return _repoWrapper.WorkSchema.GetWorkSchemaByWorkScheduleAndDay(workScheduleId, Day);
        }

        public IEnumerable<WorkSchema> GetWorkSchemaBySchedule(int workScheduleId)
        {
            return _repoWrapper.WorkSchema.GetWorkSchemaBySchedule(workScheduleId);
        }


    }
}
