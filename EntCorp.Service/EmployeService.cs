﻿using EntCorp.Contracts;
using EntCorp.Contracts.Services;
using EntCorp.Data.Models;
using System;
using System.Collections.Generic;

namespace EntCorp.Service
{
    public class EmployeService : IEmployeService
    {
        private readonly IRepositoryWrapper _repoWrapper;
        public EmployeService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public void CreateEmploye(Employe employe)
        {
            _repoWrapper.Employe.CreateEmploye(employe);
            _repoWrapper.Save();
        }

        public IEnumerable<Employe> GetAllEmployesWithDepartments()
        {
            return _repoWrapper.Employe.GetAllEmployesWithDepartments();
        }

        public IEnumerable<Employe> GetAllEmployes()
        {
            return _repoWrapper.Employe.GetAllEmployes();
        }

        public Employe GetEmployeById(int Id)
        {
            return _repoWrapper.Employe.GetEmployeById(Id);
        }

        public Employe GetEmployeByCode(string Code)
        {
            return _repoWrapper.Employe.GetEmployeByCode(Code);
        }

        public void RemoveEmploye(Employe employe)
        {
            foreach (var admission in employe.Admissions)
            {
                _repoWrapper.Admission.RemoveAdmission(admission);
            }

            _repoWrapper.Employe.RemoveEmploye(employe);
            _repoWrapper.Save();
        }

        public void UpdateEmploye(Employe employe)
        {
            _repoWrapper.Employe.UpdateEmploye(employe);
            _repoWrapper.Save();
        }

        public IEnumerable<Employe> GetEmployeForReport(DateTime RegisteredAt)
        {
            return _repoWrapper.Employe.GetEmployeForReport(RegisteredAt);
        }

        public IEnumerable<Employe> GetEmployeByDepartment(int DepartmentId)
        {
            return _repoWrapper.Employe.GetEmployeByDepartment(DepartmentId);
        }
    }
}
