﻿using EntCorp.Contracts;
using EntCorp.Contracts.Services;
using EntCorp.Data.Models;
using System.Collections.Generic;

namespace EntCorp.Service
{
    public class WorkScheduleService : IWorkScheduleService
    {
        private readonly IRepositoryWrapper _repoWrapper;
        public WorkScheduleService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public void CreateWorkSchedule(WorkSchedule workSchedule)
        {
            _repoWrapper.WorkSchedule.CreateWorkSchedule(workSchedule);
            _repoWrapper.Save();
        }

        public IEnumerable<WorkSchedule> GetAllWorkSchedules()
        {
            return _repoWrapper.WorkSchedule.GetAllWorkSchedules();
        }

        public WorkSchedule GetWorkScheduleById(int Id)
        {
            return _repoWrapper.WorkSchedule.GetWorkScheduleById(Id);
        }

        public void RemoveWorkSchedule(WorkSchedule workSchedule)
        {
            if (!workSchedule.isDefault())
            {
                _repoWrapper.WorkSchedule.RemoveWorkSchedule(workSchedule);
                _repoWrapper.Save();
            }
        }

        public void UpdateWorkSchedule(WorkSchedule workSchedule)
        {
            _repoWrapper.WorkSchedule.UpdateWorkSchedule(workSchedule);
            _repoWrapper.Save();
        }
    }
}
