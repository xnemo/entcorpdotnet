﻿using System;

namespace EntCorp.Data.Models
{
    public class Holiday : BaseEntity
    {
        public string Name { get; set; }
        public DateTime HolydayDate { get; set; }
        public DateTime? HolydayEndDate { get; set; }
        public bool Active { get; set; } = true;
    }
}
