﻿namespace EntCorp.Data.Models
{
    public class Admission : BaseEntity
    {
        public int? EmployeId { get; set; }
        public Device Device { get; set; }
        public int DeviceId { get; set; }
        public string Token { get; set; }

        public Employe Employe { get; set; }
    }
}
