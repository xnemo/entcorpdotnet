﻿using EntityFrameworkCore.Triggers;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EntCorp.Data.Models
{
    public class Department : BaseEntity
    {
        public string Name { get; set; }
        public bool Active { get; set; } = true;
        public WorkSchedule WorkSchedule { get; set; }
        public int WorkScheduleId { get; set; }
        public virtual ICollection<Employe> Employes { get; set; }

        public override string ToString()
        {
            return Name;
        }

        static Department()
        {
            Triggers<Department>.Inserting += entry =>
            {
                if (entry.Entity.WorkScheduleId == 0)
                {
                    EntCorpContext context = new EntCorpContext();
                    WorkSchedule ws = context.WorkSchedules
                        .Where(w => w.Default == true && w.Deleted != true)
                        .FirstOrDefault<WorkSchedule>()
                        ;

                    if (ws != null)
                    {
                        entry.Entity.WorkScheduleId = ws.Id;
                    }
                    else
                    {
                        MessageBox.Show("Отсутствует график работы по умолчанию");
                    }
                }
            }
            ;
        }
    }
}
