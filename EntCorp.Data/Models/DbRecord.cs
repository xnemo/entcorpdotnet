﻿namespace EntCorp.Data.Models
{
    public class DbRecord
    {
        public int Id { get; set; }
        public string Photo { get; set; }
        public string Fullname { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string FirstVisit { get; set; } = "";
        public string LastExit { get; set; } = "";
        public string WorkscheduleId { get; set; }
        public string WorkscheduleType { get; set; }
        public bool IsLate { get; set; } = false;
        public bool IsEarlyExit { get; set; } = false;
        public bool IsAbsent { get; set; } = false;
    }
}
