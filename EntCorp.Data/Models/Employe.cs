﻿using EntityFrameworkCore.Triggers;
using System;
using System.Collections.Generic;

namespace EntCorp.Data.Models
{
    public class Employe : BaseEntity
    {
        public string Fullname { get; set; }
        public DateTime? Birthdate { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; } = true;
        public bool Gender { get; set; } = true;
        public string Photo { get; set; }
        public string TableNumber { get; set; }
        public int Rank { get; set; }
        public string PassportNumber { get; set; }
        public string PassportData { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int DepartmentId { get; set; }
        public string Position { get; set; } = "";
        public int WorkScheduleId { get; set; }
        public DateTime? HiringDate { get; set; }
        public string RecruitmentOrder { get; set; }

        public virtual Department Department { get; set; }
        public virtual WorkSchedule WorkSchedule { get; set; }

        public ICollection<Admission> Admissions { get; set; }
        public ICollection<Visit> Visits { get; set; } 

        static Employe()
        {
            Triggers<Employe>.Inserting += entry =>
            {
                if (entry.Entity.WorkScheduleId == 0)
                    entry.Entity.WorkScheduleId = entry.Entity.Department.WorkScheduleId;
            }
            ;
        } 
    }
        
}
