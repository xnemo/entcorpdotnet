﻿using System;

namespace EntCorp.Data.Models
{
    public class Visit : BaseEntity
    {
        public enum Events { OUT, IN }
        public Events Event { get; set; }
        public string Code { get; set; }
        public int EmployeId { get; set; }
        public int DepartmentId { get; set; }
        public int DeviceId { get; set; }
        public DateTime RegisteredAt { get; set; }
        public string Photo { get; set; }

        public Employe Employe { get; set; }
        public virtual Department Department { get; set; }
        public virtual Device Device { get; set; }
    }
}
