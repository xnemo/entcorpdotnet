﻿using EntityFrameworkCore.Triggers;
using System;
using System.Collections.Generic;
using EntCorp.Utils;
using System.Linq;

namespace EntCorp.Data.Models
{
    public enum ReaderTypes { RFID = 0, NFC = 1, QR = 2, FINGERPRINT = 3, FACEID = 4, GATE = 5 }
    public class Device : BaseEntity
    {
        public string Name { get; set; }
        public bool Active { get; set; } = true;
        public string Token { get; set; }
        public DateTime? LastPing { get; set; }
        public string IpAdress { get; set; }
        public ReaderTypes ReaderType { get; set; }
        public string Gate { get; set; }
        public bool IsOpen { get; set; } = false;

        public virtual IEnumerable<Admission> Admissions { get; set; } = new List<Admission>();

        static Device()
        {
            Triggers<Device>.Inserting += entry =>
            {
                entry.Entity.Token = TokenHelper.GetToken(DateTime.UtcNow.ToString());
            }
            ;
        }

        public Device()
        {
            this.Active = true;
        }

        public override string ToString()
        {
            return Name; 
        }
    }
}
