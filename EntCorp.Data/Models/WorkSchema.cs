﻿namespace EntCorp.Data.Models
{
    public class WorkSchema : BaseEntity
    {
        public int? DayNumber { get; set; }
        public string StartTime { get; set; } 
        public string EndTime { get; set; }
        public float Hours { get; set; }
        public WorkSchedule WorkSchedule { get; set; }
        public int WorkScheduleId { get; set; }
    }
}
