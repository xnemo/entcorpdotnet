﻿using System.Collections.Generic;

namespace EntCorp.Data.Models
{
    public class Position : BaseEntity
    {
        public string Name { get; set; }
        //public virtual ICollection<Employe> Employes { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
