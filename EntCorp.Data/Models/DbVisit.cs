﻿using System;

namespace EntCorp.Data.Models
{
    public class DbVisit
    {
        public int Id { get; set; }
        public bool Event { get; set; }
        public DateTime RegisteredAt { get; set; }
    }
}
