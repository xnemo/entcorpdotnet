﻿//using EntCorp.Context;
using EntityFrameworkCore.Triggers;
using System.Collections.Generic;

namespace EntCorp.Data.Models
{
    public class WorkSchedule : BaseEntity
    {
        public enum Types { Свободный, Недельний, Скользящий }

        public string Name { get; set; }
        public Types Type { get; set; }
        public bool Default { get; set; } = false;

        public virtual ICollection<Employe> Employes { get; set; }
        public virtual ICollection<Department> Departments { get; set; }
        public virtual ICollection<WorkSchema> WorkSchemas { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public bool isDefault()
        {
            return Default;
        }

        static WorkSchedule()
        {
            Triggers<WorkSchedule>.Inserting += entry =>
            {
                if (entry.Entity.Default)
                {
                    //EntCorpContext context = new EntCorpContext();
                    //context.Database.ExecuteSqlCommand("UPDATE `entcorpdb`.`workschedules` SET `Default` = 0");
                }
            }
            ;
        }
    }
}
