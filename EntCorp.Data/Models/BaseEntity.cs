﻿using EntityFrameworkCore.Triggers;
using System;

namespace EntCorp.Data.Models
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public DateTime? CreatedAt { get; private set; }
        public DateTime? UpdatedAt { get; private set; }
        public bool? Deleted { get; set; }

        public bool isNew()
        {
            return Id.Equals(0);
        }

        static BaseEntity()
        {
            Triggers<BaseEntity>.Inserting += entry => entry.Entity.CreatedAt = entry.Entity.UpdatedAt = DateTime.UtcNow.ToLocalTime();
            Triggers<BaseEntity>.Updating += entry => entry.Entity.UpdatedAt = DateTime.UtcNow.ToLocalTime();
        }
    }
}
