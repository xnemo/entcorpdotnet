﻿using System;

namespace EntCorp.Data.Models
{
    public enum Roles { ADMIN, USER, MONITORING }
    public class User : BaseEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public DateTime? LastLogin { get; set; }
        public Roles Role { get; set; }
    }
}
