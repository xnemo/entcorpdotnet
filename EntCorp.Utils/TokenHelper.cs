﻿using System.Security.Cryptography;
using System.Text;

namespace EntCorp.Utils
{
    public class TokenHelper
    {
        private static string GetHash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        public static string GetToken(string input)
        {   
            return GetHash(input).Substring(0, 8).ToUpper();
        }

        public static string GetSalt(string input)
        {
            return GetHash(input);
        }

        public static string GetSaltedPassword(string password, string salt)
        {
            return GetHash(password + salt);
        }
    }
}
